<?php

namespace App\Repository;

use App\Entity\Odd;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Odd|null find($id, $lockMode = null, $lockVersion = null)
 * @method Odd|null findOneBy(array $criteria, array $orderBy = null)
 * @method Odd[]    findAll()
 * @method Odd[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OddRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Odd::class);
    }

    // /**
    //  * @return TAction[] Returns an array of TAction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TAction
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
