<?php

namespace App\Repository;

use App\Entity\TypeCible;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeCible|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeCible|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeCible[]    findAll()
 * @method TypeCible[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeCibleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeCible::class);
    }

    // /**
    //  * @return TypeCible[] Returns an array of TAction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeCible
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
