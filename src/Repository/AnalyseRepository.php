<?php

namespace App\Repository;

use App\Entity\Analyse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Analyse|null find($id, $lockMode = null, $lockVersion = null)
 * @method Analyse|null findOneBy(array $criteria, array $orderBy = null)
 * @method Analyse[]    findAll()
 * @method Analyse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnalyseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Analyse::class);
    }



    public function findActiviteAnalyseByExercice($value)
    {
        return $this->createQueryBuilder('anal')
            ->addSelect('activ')
            ->addSelect('exo')
            ->join('anal.analActivite', 'activ')
            ->join('anal.analExercice', 'exo')
            ->andWhere('anal.analExercice = :exer')
            ->setParameter('exer', $value)
//            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    public function deleteActiviteAnalyseByExercice($value)
    {
        return $this->_em->createQuery('DELETE
           FROM AppBundle:Analyse Anal
           WHERE Anal.analExercice = :exerci
           '
        )
            ->setParameter('exerci', $value)
            ->getResult();
    }


    public function sumActiviteByExerciceByMinistere($exerci,$statut,$minis)
    {
        return $this->_em->createQuery('SELECT sum(bud.budMont) as total
            FROM AppBundle:Activite activ,AppBundle:Action act,AppBundle:Programme prog,AppBundle:Budget bud
            WHERE activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND prog.progMinCod = :minis
            AND bud.budExercice = :exerci
            AND bud.budStatCod = :statut
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('minis', $minis)
            ->getResult();
    }


    // /**
    //  * @return TAction[] Returns an array of TAction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TAction
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
