<?php

namespace App\Repository;

use App\Entity\Execution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Execution|null find($id, $lockMode = null, $lockVersion = null)
 * @method Execution|null findOneBy(array $criteria, array $orderBy = null)
 * @method Execution[]    findAll()
 * @method Execution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExecutionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Execution::class);
    }



    public function findMontantParTypeCibleByExerciceByMinistereByTrimestre($exerci, $statut, $minis, $typecible, $trimes)
    {
        return $this->_em->createQuery('SELECT typcib.typCibCod, SUM(bud.budMont) as montTypeCible
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud,App:TypeCible typcib,App:Trimestre tri
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND anal.analTrimestre= tri.trimCod
            AND cib.cibCibTyp= typcib.typCibCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND prog.progMinCod = :minis
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
            AND typcib.typCibCod = :typecible
            AND tri.trimCod = :trimes
            GROUP BY typcib.typCibCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('minis', $minis)
            ->setParameter('typecible', $typecible)
            ->setParameter('trimes', $trimes)
            ->getResult();
    }

    public function findMontantParCibleOddByExerciceByMinistereByTrimestre($exerci, $statut, $minis, $odd, $trimes)
    {
        return $this->_em->createQuery('SELECT cib.cibCod, SUM(bud.budMont) as montCible
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud,App:Trimestre tri
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND anal.analTrimestre= tri.trimCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND prog.progMinCod = :minis
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
            AND cib.cibOddCod = :odd
            AND tri.trimCod = :trimes
            GROUP BY cib.cibCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('minis', $minis)
            ->setParameter('odd', $odd)
            ->setParameter('trimes', $trimes)
            ->getResult();
    }


    public function findMontantParOddByExerciceByMinistereByTrimestre($exerci, $statut, $minis, $trimes)
    {
        return $this->_em->createQuery('SELECT od.oddCod,od.oddCod, SUM(bud.budMont) as montOdd
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud,App:Trimestre tri
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND anal.analTrimestre= tri.trimCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND prog.progMinCod = :minis
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
            AND tri.trimCod = :trimes
            GROUP BY od.oddCod,od.oddCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('minis', $minis)
            ->setParameter('trimes', $trimes)
            ->getResult();
    }


    public function sumMontantParOddByExerciceByMinistereByTrimestre($exerci, $statut, $minis, $trimes)
    {
        return $this->_em->createQuery('SELECT SUM(bud.budMont) as montOdd
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud,App:Trimestre tri
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND anal.analTrimestre= tri.trimCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND prog.progMinCod = :minis
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
            AND tri.trimCod = :trimes
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('minis', $minis)
            ->setParameter('trimes', $trimes)
            ->getResult();
    }

    public function sumMontantParTypeCibleByExercice($exerci, $statut, $typecible)
    {
        return $this->_em->createQuery('SELECT SUM(bud.budMont) as montTypeCible
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud,App:TypeCible typcib
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND cib.cibCibTyp= typcib.typCibCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
            AND typcib.typCibCod = :typecible
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('typecible', $typecible)
            ->getResult();
    }

    public function findMontantParTypeCibleByExerciceByMinistere($exerci, $statut, $minis, $typecible)
    {
        return $this->_em->createQuery('SELECT typcib.typCibCod, SUM(bud.budMont) as montTypeCible
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud,App:TypeCible typcib
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND cib.cibCibTyp= typcib.typCibCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND prog.progMinCod = :minis
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
            AND typcib.typCibCod = :typecible
            GROUP BY typcib.typCibCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('minis', $minis)
            ->setParameter('typecible', $typecible)
            ->getResult();
    }


    public function findMontantParTypeCibleByExerciceAllMinistere($exerci, $statut, $typecible)
    {
        return $this->_em->createQuery('SELECT typcib.typCibCod, SUM(bud.budMont) as montTypeCible
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud,App:TypeCible typcib
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND cib.cibCibTyp= typcib.typCibCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
            AND typcib.typCibCod = :typecible
            GROUP BY typcib.typCibCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('typecible', $typecible)
            ->getResult();
    }

    public function findMontantParCibleOddByExerciceByMinistere($exerci, $statut, $minis, $odd)
    {
        return $this->_em->createQuery('SELECT cib.cibCod, SUM(bud.budMont) as montCible
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND prog.progMinCod = :minis
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
            AND cib.cibOddCod = :odd
            GROUP BY cib.cibCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('minis', $minis)
            ->setParameter('odd', $odd)
            ->getResult();
    }


    public function findMontantParCibleOddByExerciceAllMinistere($exerci, $statut, $odd)
    {
        return $this->_em->createQuery('SELECT cib.cibCod, SUM(bud.budMont) as montCible
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
            AND cib.cibOddCod = :odd
            GROUP BY cib.cibCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('odd', $odd)
            ->getResult();
    }

    public function findMontantParOddByExerciceByMinistere($exerci, $statut, $minis)
    {
        return $this->_em->createQuery('SELECT od.oddCod,od.oddCod, SUM(bud.budMont) as montOdd
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND prog.progMinCod = :minis
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
            GROUP BY od.oddCod,od.oddCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('minis', $minis)
            ->getResult();
    }


    public function findMontantParOddByExerciceAllMinistere($exerci, $statut)
    {
        return $this->_em->createQuery('SELECT od.oddCod,od.oddCod, SUM(bud.budMont) as montOdd
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
            GROUP BY od.oddCod,od.oddCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->getResult();
    }

    public function sumMontantParOddByExerciceByMinistere($exerci, $statut, $minis)
    {
        return $this->_em->createQuery('SELECT SUM(bud.budMont) as montOdd
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND prog.progMinCod = :minis
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('minis', $minis)
            ->getResult();
    }


    public function sumMontantParOddByExercice($exerci, $statut)
    {
        return $this->_em->createQuery('SELECT SUM(bud.budMont) as montOdd
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->getResult();
    }

    public function sumMontantParOddByExerciceAllMinstere($exerci, $statut)
    {
        return $this->_em->createQuery('SELECT SUM(bud.budMont) as montOdd
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->getResult();
    }

    public function findMontantParOddByExercice($exerci, $statut)
    {
        return $this->_em->createQuery('SELECT od.oddCod,od.oddCod, SUM(bud.budMont) as montOdd
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Odd od,App:Cible cib,App:Budget bud
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND cib.cibOddCod= od.oddCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
            GROUP BY od.oddCod,od.oddCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->getResult();
    }



    public function findExecutionByAnalyse($analys)
    {
        return $this->_em->createQuery('SELECT execu.id,execu.execMont,trimes.trimLib
            FROM App:Execution execu,App:Trimestre trimes
            WHERE execu.execTrimestre =trimes.trimCod
            AND execu.execAnalyse= :analys
           '
        )
            ->setParameter('analys', $analys)
            ->getResult();
    }


    public function findActiviteExecutionByAnalyse($analys)
    {
        return $this->_em->createQuery('SELECT DISTINCT anal.id, cib.cibCod, activ.activLib, bud.budMont
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Cible cib,App:Budget bud
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.id = :analys
           '
        )
            ->setParameter('analys', $analys)
            ->getResult();
    }


    public function findActiviteExecutionByExerciceByMinistere($exerci, $statut, $minis)
    {
        return $this->_em->createQuery('SELECT DISTINCT anal.id, cib.cibCod, activ.activLib, bud.budMont
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Cible cib,App:Budget bud
            WHERE anal.analActivite = activ.activCod
            AND anal.analCible= cib.cibCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND prog.progMinCod = :minis
            AND anal.analExercice = :exerci
            AND bud.budStatCod = :statut
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('minis', $minis)
            ->getResult();
    }


    public function sumActiviteExecutionByExerciceByMinistere($exerci, $statut, $minis)
    {
        return $this->_em->createQuery('SELECT sum(bud.budMont) as total
            FROM App:Activite activ,App:Action act,App:Programme prog,App:Budget bud
            WHERE activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND prog.progMinCod = :minis
            AND bud.budExercice = :exerci
            AND bud.budStatCod = :statut
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('minis', $minis)
            ->getResult();
    }


    public function sumActiviteExecutionByExerciceAllMinistere($exerci, $statut)
    {
        return $this->_em->createQuery('SELECT sum(bud.budMont) as total
            FROM App:Activite activ,App:Action act,App:Programme prog,App:Budget bud
            WHERE activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND bud.budExercice = :exerci
            AND bud.budStatCod = :statut
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->getResult();
    }


    public function sumActiviteExecutionByExercice($exerci, $statut)
    {
        return $this->_em->createQuery('SELECT sum(bud.budMont) as total
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Budget bud,App:Ministere minis
            WHERE activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analActivite = activ.activCod
            AND prog.progMinCod = minis.minCod
            AND bud.budExercice = :exerci
            AND bud.budStatCod = :statut
            GROUP BY prog.progMinCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->getResult();
    }

    public function findActiviteExecutionByExerciceByMinistereByAnalyseParMinistere($exerci, $statut)
    {
        return $this->_em->createQuery('SELECT minis.minCod, minis.minLibCrt, minis.minLibLong, sum(bud.budMont) as total
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Budget bud,App:Ministere minis
            WHERE activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analActivite = activ.activCod
            AND prog.progMinCod = minis.minCod
            AND bud.budExercice = :exerci
            AND bud.budStatCod = :statut
            GROUP BY prog.progMinCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->getResult();
    }


    public function sumActiviteExecutionByExerciceByAnalyse($exerci, $statut)
    {
        return $this->_em->createQuery('SELECT sum(bud.budMont) as total
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Budget bud,App:Ministere minis
            WHERE activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analActivite = activ.activCod
            AND prog.progMinCod = minis.minCod
            AND bud.budExercice = :exerci
            AND bud.budStatCod = :statut
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->getResult();
    }

    public function sumActiviteExecutionByExerciceByMinistereByAnalyse($exerci, $statut, $minis)
    {
        return $this->_em->createQuery('SELECT sum(bud.budMont) as total
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Budget bud
            WHERE activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analActivite = activ.activCod
            AND prog.progMinCod = :minis
            AND bud.budExercice = :exerci
            AND bud.budStatCod = :statut
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('minis', $minis)
            ->getResult();
    }

    public function sumActiviteExecutionByExerciceByMinistereByAnalyseByTrimestre($exerci, $statut, $minis, $trim)
    {
        return $this->_em->createQuery('SELECT sum(bud.budMont) as total
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Budget bud,App:Trimestre tri
            WHERE activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analActivite = activ.activCod
            AND anal.analTrimestre = tri.trimCod
            AND prog.progMinCod = :minis
            AND bud.budExercice = :exerci
            AND bud.budStatCod = :statut
            AND tri.trimCod = :trim
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('minis', $minis)
            ->setParameter('trim', $trim)
            ->getResult();
    }


    public function sumExecutionByMinistereByProgramme($exer, $progr, $minis)
    {
        return $this->_em->createQuery('SELECT SUM(exec.execMont) as mont_exec
            FROM App:Execution exec,App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog
            WHERE exec.execAnalyse = anal.id
            AND anal.analActivite = activ.activCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND prog.progMinCod = :minis
            AND prog.progCod = :progr
            AND exec.execExercice = :exer
           '
        )
            ->setParameter('minis', $minis)
            ->setParameter('progr', $progr)
            ->setParameter('exer', $exer)
            ->getResult();
    }



    public function sumExecutionByMinistereByAction($exer, $act, $minis)
    {
        return $this->_em->createQuery('SELECT SUM(exec.execMont) as mont_exec
            FROM App:Execution exec,App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog
            WHERE exec.execAnalyse = anal.id
            AND anal.analActivite = activ.activCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND act.actCod = :act
            AND prog.progMinCod = :minis
            AND exec.execExercice = :exer
           '
        )
            ->setParameter('minis', $minis)
            ->setParameter('act', $act)
            ->setParameter('exer', $exer)
            ->getResult();
    }




    public function sumExecutionByMinistereByActivite($exer, $activ, $minis)
    {
        return $this->_em->createQuery('SELECT SUM(exec.execMont) as mont_exec
            FROM App:Execution exec,App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog
            WHERE exec.execAnalyse = anal.id
            AND anal.analActivite = activ.activCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND activ.activCod = :activ
            AND prog.progMinCod = :minis
            AND exec.execExercice = :exer
           '
        )
            ->setParameter('minis', $minis)
            ->setParameter('activ', $activ)
            ->setParameter('exer', $exer)
            ->getResult();
    }


    public function sumExecutionByMinistere($minis, $exer)
    {
        return $this->_em->createQuery('SELECT SUM(exec.execMont) as mont_exec
            FROM App:Execution exec,App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog
            WHERE exec.execAnalyse = anal.id
            AND anal.analActivite = activ.activCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND prog.progMinCod = :minis
            AND exec.execExercice = :exer
           '
        )
            ->setParameter('minis', $minis)
            ->setParameter('exer', $exer)
            ->getResult();
    }


/*    public function sumExecutionByExo($exer)
    {
        return $this->_em->createQuery('SELECT prog.progCod,SUM(exec.execMont) as mont_exec
            FROM App:Execution exec,App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog
            WHERE exec.execAnalyse = anal.id
            AND anal.analActivite = activ.activCod
            AND activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND exec.execExercice = :exer
            GROUP BY prog.progCod
           '
        )
            ->setParameter('exer', $exer)
            ->getResult();
    }*/


    public function sumExecutionByBudget($anal)
    {
        return $this->_em->createQuery('SELECT SUM(exec.execMont) as mont_exec
            FROM App:Execution exec
            WHERE exec.execAnalyse = :anal
           '
        )
            ->setParameter('anal', $anal)
            ->getResult();
    }


    public function deleteActiviteExecutionByExerciceByMinistere($value)
    {
        return $this->_em->createQuery('DELETE
           FROM App:Budget bud
           WHERE bud.budExercice = :exerci
           '
        )
            ->setParameter('exerci', $value)
            ->getResult();
    }

    public function valideActiviteExecutionByExerciceByMinistere($value)
    {
        return $this->_em->createQuery('UPDATE App:Budget bud 
            SET bud.budStatCod=:statut
           WHERE bud.budExercice = :exerci
           '
        )
            ->setParameter('exerci', $value)
            ->setParameter('statut', "BSV")
            ->getResult();
    }
    // /**
    //  * @return TAction[] Returns an array of TAction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TAction
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
