<?php

namespace App\Repository;

use App\Entity\Budget;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Budget|null find($id, $lockMode = null, $lockVersion = null)
 * @method Budget|null findOneBy(array $criteria, array $orderBy = null)
 * @method Budget[]    findAll()
 * @method Budget[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BudgetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Budget::class);
    }

    public function findActiviteBudgetByExercice($value,$statut)
    {
        return $this->createQueryBuilder('bud')
            ->addSelect('activ')
            ->addSelect('exo')
            ->join('bud.budActivite', 'activ')
            ->join('bud.budExercice', 'exo')
            ->andWhere('bud.budExercice = :exer')
            ->andWhere('bud.budStatCod = :statut')
            ->setParameter('exer', $value)
            ->setParameter('statut', $statut)
//            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
//        return $this->_em->createQuery('SELECT bud
//            FROM AppBundle:Budget bud, AppBundle:Activite activ
//            WHERE activ.id = bud.budActivite
//            AND bud.budExercice = :exerci
//            '
//        )
//            ->setParameter('exerci', $value)
//            ->getResult();
    }

    public function deleteActiviteBudgetByExercice($value)
    {
        return $this->_em->createQuery('DELETE
           FROM App:Budget bud
           WHERE bud.budExercice = :exerci
           '
        )
            ->setParameter('exerci', $value)
            ->getResult();
    }

    public function valideActiviteBudgetByExercice($value)
    {
        return $this->_em->createQuery('UPDATE App:Budget bud 
            SET bud.budStatCod=:statut
           WHERE bud.budExercice = :exerci
           '
        )
            ->setParameter('exerci', $value)
            ->setParameter('statut', "BSV")
            ->getResult();
    }


    public function findMinistereByExercice($exerci, $statut)
    {
        return $this->_em->createQuery('SELECT minis.minCod, minis.minLibCrt, minis.minLibLong, sum(bud.budMont) as total
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Budget bud,App:Ministere minis
            WHERE activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analActivite = activ.activCod
            AND prog.progMinCod = minis.minCod
            AND bud.budExercice = :exerci
            AND bud.budStatCod = :statut
            GROUP BY minis.minCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->getResult();
    }



    public function findProgrammeMinistereByExercice($exerci, $statut, $ministere)
    {
        return $this->_em->createQuery('SELECT prog.progCod, prog.progLib, sum(bud.budMont) as total
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Budget bud,App:Ministere minis
            WHERE activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analActivite = activ.activCod
            AND prog.progMinCod = minis.minCod
            AND bud.budExercice = :exerci
            AND bud.budStatCod = :statut
            AND minis.minCod = :min
            GROUP BY prog.progCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('min', $ministere)
            ->getResult();
    }



    public function findActionProgrammeMinistereByExercice($exerci, $statut, $ministere, $programme)
    {
        return $this->_em->createQuery('SELECT act.actCod, act.actLib, sum(bud.budMont) as total
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Budget bud,App:Ministere minis
            WHERE activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analActivite = activ.activCod
            AND prog.progMinCod = minis.minCod
            AND bud.budExercice = :exerci
            AND bud.budStatCod = :statut
            AND minis.minCod = :min
            AND prog.progCod = :pro
            GROUP BY act.actCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('min', $ministere)
            ->setParameter('pro', $programme)
            ->getResult();
    }


    public function findActiviteActionProgrammeMinistereByExercice($exerci, $statut, $ministere, $programme, $action)
    {
        return $this->_em->createQuery('SELECT activ.activCod, activ.activLib, sum(bud.budMont) as total
            FROM App:Analyse anal,App:Activite activ,App:Action act,App:Programme prog,App:Budget bud,App:Ministere minis
            WHERE activ.activActCod = act.actCod
            AND act.actProgCod = prog.progCod
            AND bud.budActivite = activ.activCod
            AND anal.analActivite = activ.activCod
            AND prog.progMinCod = minis.minCod
            AND bud.budExercice = :exerci
            AND bud.budStatCod = :statut
            AND minis.minCod = :min
            AND prog.progCod = :pro
            AND act.actCod = :acti
            GROUP BY activ.activCod
           '
        )
            ->setParameter('exerci', $exerci)
            ->setParameter('statut', $statut)
            ->setParameter('min', $ministere)
            ->setParameter('pro', $programme)
            ->setParameter('acti', $action)
            ->getResult();
    }

    // /**
    //  * @return TAction[] Returns an array of TAction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TAction
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
