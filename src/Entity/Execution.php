<?php

namespace App\Entity;

use App\Repository\ExecutionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExecutionRepository::class)
 */
class Execution
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="float")
     */
    private $execMont;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Exercice", cascade={"persist"})
     * @ORM\JoinColumn(name="exec_exe_num", referencedColumnName="exe_num")
     */
    private $execExercice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Trimestre", cascade={"persist"})
     * @ORM\JoinColumn(name="exec_trim_cod", referencedColumnName="trim_cod")
     */
    private $execTrimestre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Analyse", cascade={"persist"})
     * @ORM\JoinColumn(name="exec_analyse", referencedColumnName="id")
     */
    private $execAnalyse;

    /**
     *
     * @ORM\Column(type="string")
     */
    private $execStatCod;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $execUserSai;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $execUserVal;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $execDatSai;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $execDatVal;


    public function __construct()
    {
        $this->execStatCod = "EBSN";
        $this->execDatSai = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getExecMont()
    {
        return $this->execMont;
    }

    /**
     * @param mixed $execMont
     */
    public function setExecMont($execMont): void
    {
        $this->execMont = $execMont;
    }

    /**
     * @return mixed
     */
    public function getExecExercice()
    {
        return $this->execExercice;
    }

    /**
     * @param mixed $execExercice
     */
    public function setExecExercice($execExercice): void
    {
        $this->execExercice = $execExercice;
    }

    /**
     * @return mixed
     */
    public function getExecTrimestre()
    {
        return $this->execTrimestre;
    }

    /**
     * @param mixed $execTrimestre
     */
    public function setExecTrimestre($execTrimestre): void
    {
        $this->execTrimestre = $execTrimestre;
    }

    /**
     * @return mixed
     */
    public function getExecAnalyse()
    {
        return $this->execAnalyse;
    }

    /**
     * @param mixed $execAnalyse
     */
    public function setExecAnalyse($execAnalyse): void
    {
        $this->execAnalyse = $execAnalyse;
    }

    /**
     * @return mixed
     */
    public function getExecStatCod()
    {
        return $this->execStatCod;
    }

    /**
     * @param mixed $execStatCod
     */
    public function setExecStatCod($execStatCod): void
    {
        $this->execStatCod = $execStatCod;
    }

    /**
     * @return mixed
     */
    public function getExecUserSai()
    {
        return $this->execUserSai;
    }

    /**
     * @param mixed $execUserSai
     */
    public function setExecUserSai($execUserSai): void
    {
        $this->execUserSai = $execUserSai;
    }

    /**
     * @return mixed
     */
    public function getExecUserVal()
    {
        return $this->execUserVal;
    }

    /**
     * @param mixed $execUserVal
     */
    public function setExecUserVal($execUserVal): void
    {
        $this->execUserVal = $execUserVal;
    }

    /**
     * @return mixed
     */
    public function getExecDatSai()
    {
        return $this->execDatSai;
    }

    /**
     * @param mixed $execDatSai
     */
    public function setExecDatSai($execDatSai): void
    {
        $this->execDatSai = $execDatSai;
    }

    /**
     * @return mixed
     */
    public function getExecDatVal()
    {
        return $this->execDatVal;
    }

    /**
     * @param mixed $execDatVal
     */
    public function setExecDatVal($execDatVal): void
    {
        $this->execDatVal = $execDatVal;
    }
    


}
