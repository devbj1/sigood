<?php

namespace App\Entity;

use App\Repository\ExerciceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExerciceRepository::class)
 */
class Exercice
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=4)
     */
    private $exeNum;

    /**
     *
     * @ORM\Column(type="datetime")
     */
    private $exeDatDeb;

    /**
     *
     * @ORM\Column(type="datetime")
     */
    private $exeDatFin;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $exeEncours;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $exeUserSai;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $exeUserModif;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $exeDatSai;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $exeDatModif;


    public function __construct()
    {
        $this->exeEncours = false;
        $this->exeDatSai = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getExeNum()
    {
        return $this->exeNum;
    }

    /**
     * @param mixed $exeNum
     */
    public function setExeNum($exeNum): void
    {
        $this->exeNum = $exeNum;
    }

    /**
     * @return mixed
     */
    public function getExeDatDeb()
    {
        return $this->exeDatDeb;
    }

    /**
     * @param mixed $exeDatDeb
     */
    public function setExeDatDeb($exeDatDeb): void
    {
        $this->exeDatDeb = $exeDatDeb;
    }

    /**
     * @return mixed
     */
    public function getExeDatFin()
    {
        return $this->exeDatFin;
    }

    /**
     * @param mixed $exeDatFin
     */
    public function setExeDatFin($exeDatFin): void
    {
        $this->exeDatFin = $exeDatFin;
    }

    /**
     * @return mixed
     */
    public function getExeEncours()
    {
        return $this->exeEncours;
    }

    /**
     * @param mixed $exeEncours
     */
    public function setExeEncours($exeEncours): void
    {
        $this->exeEncours = $exeEncours;
    }

    /**
     * @return mixed
     */
    public function getExeUserSai()
    {
        return $this->exeUserSai;
    }

    /**
     * @param mixed $exeUserSai
     */
    public function setExeUserSai($exeUserSai): void
    {
        $this->exeUserSai = $exeUserSai;
    }

    /**
     * @return mixed
     */
    public function getExeUserModif()
    {
        return $this->exeUserModif;
    }

    /**
     * @param mixed $exeUserModif
     */
    public function setExeUserModif($exeUserModif): void
    {
        $this->exeUserModif = $exeUserModif;
    }

    /**
     * @return mixed
     */
    public function getExeDatSai()
    {
        return $this->exeDatSai;
    }

    /**
     * @param mixed $exeDatSai
     */
    public function setExeDatSai($exeDatSai): void
    {
        $this->exeDatSai = $exeDatSai;
    }

    /**
     * @return mixed
     */
    public function getExeDatModif()
    {
        return $this->exeDatModif;
    }

    /**
     * @param mixed $exeDatModif
     */
    public function setExeDatModif($exeDatModif): void
    {
        $this->exeDatModif = $exeDatModif;
    }


}
