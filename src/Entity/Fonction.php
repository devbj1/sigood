<?php

namespace App\Entity;

use App\Repository\FonctionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FonctionRepository::class)
 */
class Fonction
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=10, unique=true, nullable=true)
     */
    private $fonCod;

    /**
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $fonLib;

    /**
     *
     * @ORM\Column(type="boolean")
     */
    private $fonActif;

    public function __construct()
    {
        $this->fonActif = false;
    }

    /**
     * @return mixed
     */
    public function getFonCod()
    {
        return $this->fonCod;
    }

    /**
     * @param mixed $fonCod
     */
    public function setFonCod($fonCod): void
    {
        $this->fonCod = $fonCod;
    }

    /**
     * @return string
     */
    public function getFonLib(): string
    {
        return $this->fonLib;
    }

    /**
     * @param string $fonLib
     */
    public function setFonLib(string $fonLib): void
    {
        $this->fonLib = $fonLib;
    }

    /**
     * @return bool
     */
    public function isFonActif(): bool
    {
        return $this->fonActif;
    }

    /**
     * @param bool $fonActif
     */
    public function setFonActif(bool $fonActif): void
    {
        $this->fonActif = $fonActif;
    }


}
