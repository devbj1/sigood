<?php

namespace App\Entity;

use App\Repository\ParametreRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParametreRepository::class)
 */
class Parametre
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\Column(name="param_cod", type="string", length=30)
     */
    private $paramCod;

    /**
     *
     * @ORM\Column(type="string", length=500)
     */
    private $paramLibelle;

    /**
     *
     * @ORM\Column(type="datetime")
     */
    private $paramDateEffet;

    /**
     *
     * @ORM\Column(type="float")
     */
    private $paramValeurInt;

    /**
     *
     * @ORM\Column(type="string", length=500)
     */
    private $paramValeurString;

    /**
     *
     * @ORM\Column(type="string", length=500)
     */
    private $paramValeurImage;

    /**
     *
     * @ORM\Column(type="boolean")
     */
    private $paramModifiable;


    public function __construct()
    {
        $this->paramModifiable = false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getParamCod()
    {
        return $this->paramCod;
    }

    /**
     * @param mixed $paramCod
     */
    public function setParamCod($paramCod): void
    {
        $this->paramCod = $paramCod;
    }

    /**
     * @return mixed
     */
    public function getParamLibelle()
    {
        return $this->paramLibelle;
    }

    /**
     * @param mixed $paramLibelle
     */
    public function setParamLibelle($paramLibelle): void
    {
        $this->paramLibelle = $paramLibelle;
    }

    /**
     * @return mixed
     */
    public function getParamDateEffet()
    {
        return $this->paramDateEffet;
    }

    /**
     * @param mixed $paramDateEffet
     */
    public function setParamDateEffet($paramDateEffet): void
    {
        $this->paramDateEffet = $paramDateEffet;
    }

    /**
     * @return mixed
     */
    public function getParamValeurInt()
    {
        return $this->paramValeurInt;
    }

    /**
     * @param mixed $paramValeurInt
     */
    public function setParamValeurInt($paramValeurInt): void
    {
        $this->paramValeurInt = $paramValeurInt;
    }

    /**
     * @return mixed
     */
    public function getParamValeurString()
    {
        return $this->paramValeurString;
    }

    /**
     * @param mixed $paramValeurString
     */
    public function setParamValeurString($paramValeurString): void
    {
        $this->paramValeurString = $paramValeurString;
    }

    /**
     * @return mixed
     */
    public function getParamValeurImage()
    {
        return $this->paramValeurImage;
    }

    /**
     * @param mixed $paramValeurImage
     */
    public function setParamValeurImage($paramValeurImage): void
    {
        $this->paramValeurImage = $paramValeurImage;
    }

    /**
     * @return mixed
     */
    public function getParamModifiable()
    {
        return $this->paramModifiable;
    }

    /**
     * @param mixed $paramModifiable
     */
    public function setParamModifiable($paramModifiable): void
    {
        $this->paramModifiable = $paramModifiable;
    }

}
