<?php

namespace App\Entity;

use App\Repository\TypeCibleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeCibleRepository::class)
 */
class TypeCible
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=10)
     */
    private $typCibCod;

    /**
     *
     * @ORM\Column(type="string", length=500)
     */
    private $typCibLib;

    /**
     * @var bool
     *
     * @ORM\Column(name="ty_cacher", type="boolean", nullable=true)
     */
    private $typCacher = false;

    /**
     * @return mixed
     */
    public function getTypCibCod()
    {
        return $this->typCibCod;
    }

    /**
     * @param mixed $typCibCod
     */
    public function setTypCibCod($typCibCod): void
    {
        $this->typCibCod = $typCibCod;
    }

    /**
     * @return mixed
     */
    public function getTypCibLib()
    {
        return $this->typCibLib;
    }

    /**
     * @param mixed $typCibLib
     */
    public function setTypCibLib($typCibLib): void
    {
        $this->typCibLib = $typCibLib;
    }

    /**
     * @return bool
     */
    public function isTypCacher(): bool
    {
        return $this->typCacher;
    }

    /**
     * @param bool $typCacher
     */
    public function setTypCacher(bool $typCacher): void
    {
        $this->typCacher = $typCacher;
    }


}
