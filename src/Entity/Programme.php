<?php

namespace App\Entity;

use App\Repository\ProgrammeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProgrammeRepository::class)
 */
class Programme
{
    /**
     * @ORM\Id
     *
     * @ORM\Column(type="string", length=10)
     */
    private $progCod;

    /**
     *
     * @ORM\Column(type="string", length=500)
     */
    private $progLib;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $progDes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ministere", cascade={"persist"})
     * @ORM\JoinColumn(name="prog_min_cod", referencedColumnName="min_cod")
     */
    private $progMinCod;


    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $progUserSai;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $progUserModif;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $progDatSai;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $progDatModif;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $progCacher;

    public function __construct()
    {
        $this->progCacher = false;
        $this->progDatSai = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getProgCod()
    {
        return $this->progCod;
    }

    /**
     * @param mixed $progCod
     */
    public function setProgCod($progCod): void
    {
        $this->progCod = $progCod;
    }

    /**
     * @return mixed
     */
    public function getProgLib()
    {
        return $this->progLib;
    }

    /**
     * @param mixed $progLib
     */
    public function setProgLib($progLib): void
    {
        $this->progLib = $progLib;
    }

    /**
     * @return mixed
     */
    public function getProgDes()
    {
        return $this->progDes;
    }

    /**
     * @param mixed $progDes
     */
    public function setProgDes($progDes): void
    {
        $this->progDes = $progDes;
    }

    /**
     * @return mixed
     */
    public function getProgMinCod()
    {
        return $this->progMinCod;
    }

    /**
     * @param mixed $progMinCod
     */
    public function setProgMinCod($progMinCod): void
    {
        $this->progMinCod = $progMinCod;
    }

    /**
     * @return mixed
     */
    public function getProgUserSai()
    {
        return $this->progUserSai;
    }

    /**
     * @param mixed $progUserSai
     */
    public function setProgUserSai($progUserSai): void
    {
        $this->progUserSai = $progUserSai;
    }

    /**
     * @return mixed
     */
    public function getProgUserModif()
    {
        return $this->progUserModif;
    }

    /**
     * @param mixed $progUserModif
     */
    public function setProgUserModif($progUserModif): void
    {
        $this->progUserModif = $progUserModif;
    }

    /**
     * @return mixed
     */
    public function getProgDatSai()
    {
        return $this->progDatSai;
    }

    /**
     * @param mixed $progDatSai
     */
    public function setProgDatSai($progDatSai): void
    {
        $this->progDatSai = $progDatSai;
    }

    /**
     * @return mixed
     */
    public function getProgDatModif()
    {
        return $this->progDatModif;
    }

    /**
     * @param mixed $progDatModif
     */
    public function setProgDatModif($progDatModif): void
    {
        $this->progDatModif = $progDatModif;
    }

    /**
     * @return mixed
     */
    public function getProgCacher()
    {
        return $this->progCacher;
    }

    /**
     * @param mixed $progCacher
     */
    public function setProgCacher($progCacher): void
    {
        $this->progCacher = $progCacher;
    }

}
