<?php

namespace App\Entity;

use App\Repository\CibleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CibleRepository::class)
 */
class Cible
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=10)
     */
    private $cibCod;

    /**
     *
     * @ORM\Column(type="string", length=500)
     */
    private $cibLib;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Odd", cascade={"persist"})
     * @ORM\JoinColumn(name="cib_odd_cod", referencedColumnName="odd_cod")
     */
    private $cibOddCod;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable = true)
     */
    private $cibUserSai;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable = true)
     */
    private $cibUserModif;

    /**
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $cibDatSai;

    /**
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $cibDatModif;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeCible", cascade={"persist"})
     * @ORM\JoinColumn(name="cib_cib_typ", referencedColumnName="typ_cib_cod")
     */
    private $cibCibTyp;

    /**
     *
     * @ORM\Column(type="boolean", nullable = true)
     */
    private $cibCacher;


    public function __construct()
    {
        $this->cibCacher = false;
        $this->cibDatSai = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getCibCod()
    {
        return $this->cibCod;
    }

    /**
     * @param mixed $cibCod
     */
    public function setCibCod($cibCod): void
    {
        $this->cibCod = $cibCod;
    }

    /**
     * @return mixed
     */
    public function getCibLib()
    {
        return $this->cibLib;
    }

    /**
     * @param mixed $cibLib
     */
    public function setCibLib($cibLib): void
    {
        $this->cibLib = $cibLib;
    }

    /**
     * @return mixed
     */
    public function getCibOddCod()
    {
        return $this->cibOddCod;
    }

    /**
     * @param mixed $cibOddCod
     */
    public function setCibOddCod($cibOddCod): void
    {
        $this->cibOddCod = $cibOddCod;
    }

    /**
     * @return mixed
     */
    public function getCibUserSai()
    {
        return $this->cibUserSai;
    }

    /**
     * @param mixed $cibUserSai
     */
    public function setCibUserSai($cibUserSai): void
    {
        $this->cibUserSai = $cibUserSai;
    }

    /**
     * @return mixed
     */
    public function getCibUserModif()
    {
        return $this->cibUserModif;
    }

    /**
     * @param mixed $cibUserModif
     */
    public function setCibUserModif($cibUserModif): void
    {
        $this->cibUserModif = $cibUserModif;
    }

    /**
     * @return mixed
     */
    public function getCibDatSai()
    {
        return $this->cibDatSai;
    }

    /**
     * @param mixed $cibDatSai
     */
    public function setCibDatSai($cibDatSai): void
    {
        $this->cibDatSai = $cibDatSai;
    }

    /**
     * @return mixed
     */
    public function getCibDatModif()
    {
        return $this->cibDatModif;
    }

    /**
     * @param mixed $cibDatModif
     */
    public function setCibDatModif($cibDatModif): void
    {
        $this->cibDatModif = $cibDatModif;
    }

    /**
     * @return mixed
     */
    public function getCibCibTyp()
    {
        return $this->cibCibTyp;
    }

    /**
     * @param mixed $cibCibTyp
     */
    public function setCibCibTyp($cibCibTyp): void
    {
        $this->cibCibTyp = $cibCibTyp;
    }

    /**
     * @return mixed
     */
    public function getCibCacher()
    {
        return $this->cibCacher;
    }

    /**
     * @param mixed $cibCacher
     */
    public function setCibCacher($cibCacher): void
    {
        $this->cibCacher = $cibCacher;
    }



}
