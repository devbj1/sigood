<?php

namespace App\Entity;

use App\Repository\OddRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OddRepository::class)
 */
class Odd
{

    /**
     * @ORM\Id
     *
     * @ORM\Column(type="string", length=10)
     */
    private $oddCod;

    /**
     *
     * @ORM\Column(type="string", length=500)
     */
    private $oddLib;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $oddUserSai;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $oddUserModif;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $oddDatSai;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $oddDatModif;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $oddCacher;


    public function __construct()
    {
        $this->oddCacher = false;
        $this->oddDatSai = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getOddCod()
    {
        return $this->oddCod;
    }

    /**
     * @param mixed $oddCod
     */
    public function setOddCod($oddCod): void
    {
        $this->oddCod = $oddCod;
    }

    /**
     * @return mixed
     */
    public function getOddLib()
    {
        return $this->oddLib;
    }

    /**
     * @param mixed $oddLib
     */
    public function setOddLib($oddLib): void
    {
        $this->oddLib = $oddLib;
    }

    /**
     * @return mixed
     */
    public function getOddUserSai()
    {
        return $this->oddUserSai;
    }

    /**
     * @param mixed $oddUserSai
     */
    public function setOddUserSai($oddUserSai): void
    {
        $this->oddUserSai = $oddUserSai;
    }

    /**
     * @return mixed
     */
    public function getOddUserModif()
    {
        return $this->oddUserModif;
    }

    /**
     * @param mixed $oddUserModif
     */
    public function setOddUserModif($oddUserModif): void
    {
        $this->oddUserModif = $oddUserModif;
    }

    /**
     * @return mixed
     */
    public function getOddDatSai()
    {
        return $this->oddDatSai;
    }

    /**
     * @param mixed $oddDatSai
     */
    public function setOddDatSai($oddDatSai): void
    {
        $this->oddDatSai = $oddDatSai;
    }

    /**
     * @return mixed
     */
    public function getOddDatModif()
    {
        return $this->oddDatModif;
    }

    /**
     * @param mixed $oddDatModif
     */
    public function setOddDatModif($oddDatModif): void
    {
        $this->oddDatModif = $oddDatModif;
    }

    /**
     * @return mixed
     */
    public function getOddCacher()
    {
        return $this->oddCacher;
    }

    /**
     * @param mixed $oddCacher
     */
    public function setOddCacher($oddCacher): void
    {
        $this->oddCacher = $oddCacher;
    }

}
