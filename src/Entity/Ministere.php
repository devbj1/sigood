<?php

namespace App\Entity;

use App\Repository\MinistereRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MinistereRepository::class)
 */
class Ministere
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=10)
     */
    private $minCod;

    /**
     *
     * @ORM\Column(type="string", length=100)
     */
    private $minLibCrt;

    /**
     *
     * @ORM\Column(type="string", length=300)
     */
    private $minLibLong;

    /**
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $minAdres;

    /**
     *
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $minTel;

    /**
     *
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $minEmail;

    /**
     *
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $minSite;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $minDatSai;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $minDatModif;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $minUserSai;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $minUserModif;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $minCacher;

    public function __construct()
    {
        $this->minCacher = false;
        $this->minDatSai = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getMinCod()
    {
        return $this->minCod;
    }

    /**
     * @param mixed $minCod
     */
    public function setMinCod($minCod): void
    {
        $this->minCod = $minCod;
    }

    /**
     * @return mixed
     */
    public function getMinLibCrt()
    {
        return $this->minLibCrt;
    }

    /**
     * @param mixed $minLibCrt
     */
    public function setMinLibCrt($minLibCrt): void
    {
        $this->minLibCrt = $minLibCrt;
    }

    /**
     * @return mixed
     */
    public function getMinLibLong()
    {
        return $this->minLibLong;
    }

    /**
     * @param mixed $minLibLong
     */
    public function setMinLibLong($minLibLong): void
    {
        $this->minLibLong = $minLibLong;
    }

    /**
     * @return mixed
     */
    public function getMinAdres()
    {
        return $this->minAdres;
    }

    /**
     * @param mixed $minAdres
     */
    public function setMinAdres($minAdres): void
    {
        $this->minAdres = $minAdres;
    }

    /**
     * @return mixed
     */
    public function getMinTel()
    {
        return $this->minTel;
    }

    /**
     * @param mixed $minTel
     */
    public function setMinTel($minTel): void
    {
        $this->minTel = $minTel;
    }

    /**
     * @return mixed
     */
    public function getMinEmail()
    {
        return $this->minEmail;
    }

    /**
     * @param mixed $minEmail
     */
    public function setMinEmail($minEmail): void
    {
        $this->minEmail = $minEmail;
    }

    /**
     * @return mixed
     */
    public function getMinSite()
    {
        return $this->minSite;
    }

    /**
     * @param mixed $minSite
     */
    public function setMinSite($minSite): void
    {
        $this->minSite = $minSite;
    }

    /**
     * @return mixed
     */
    public function getMinDatSai()
    {
        return $this->minDatSai;
    }

    /**
     * @param mixed $minDatSai
     */
    public function setMinDatSai($minDatSai): void
    {
        $this->minDatSai = $minDatSai;
    }

    /**
     * @return mixed
     */
    public function getMinDatModif()
    {
        return $this->minDatModif;
    }

    /**
     * @param mixed $minDatModif
     */
    public function setMinDatModif($minDatModif): void
    {
        $this->minDatModif = $minDatModif;
    }

    /**
     * @return mixed
     */
    public function getMinUserSai()
    {
        return $this->minUserSai;
    }

    /**
     * @param mixed $minUserSai
     */
    public function setMinUserSai($minUserSai): void
    {
        $this->minUserSai = $minUserSai;
    }

    /**
     * @return mixed
     */
    public function getMinUserModif()
    {
        return $this->minUserModif;
    }

    /**
     * @param mixed $minUserModif
     */
    public function setMinUserModif($minUserModif): void
    {
        $this->minUserModif = $minUserModif;
    }

    /**
     * @return mixed
     */
    public function getMinCacher()
    {
        return $this->minCacher;
    }

    /**
     * @param mixed $minCacher
     */
    public function setMinCacher($minCacher): void
    {
        $this->minCacher = $minCacher;
    }

}
