<?php

namespace App\Entity;

use App\Repository\StatutRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StatutRepository::class)
 */
class Statut
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=10)
     */
    private $statCod;

    /**
     *
     * @ORM\Column(type="string", length=100)
     */
    private $statLib;

    /**
     * @return mixed
     */
    public function getStatCod()
    {
        return $this->statCod;
    }

    /**
     * @param mixed $statCod
     */
    public function setStatCod($statCod): void
    {
        $this->statCod = $statCod;
    }

    /**
     * @return mixed
     */
    public function getStatLib()
    {
        return $this->statLib;
    }

    /**
     * @param mixed $statLib
     */
    public function setStatLib($statLib): void
    {
        $this->statLib = $statLib;
    }



}
