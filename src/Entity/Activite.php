<?php

namespace App\Entity;

use App\Repository\ActiviteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ActiviteRepository::class)
 */
class Activite
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=20)
     */
    private $activCod;

    /**
     *
     * @ORM\Column(type="string", length=500)
     */
    private $activLib;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $activDes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Action", cascade={"persist"})
     * @ORM\JoinColumn(name="activ_act_cod", referencedColumnName="act_cod")
     */
    private $activActCod;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $activUserSai;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $activUserModif;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $activDatSai;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $activDatModif;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $activCacher;


    public function __construct()
    {
        $this->activCacher = false;
        $this->activDatSai = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getActivCod()
    {
        return $this->activCod;
    }

    /**
     * @param mixed $activCod
     */
    public function setActivCod($activCod): void
    {
        $this->activCod = $activCod;
    }

    /**
     * @return mixed
     */
    public function getActivLib()
    {
        return $this->activLib;
    }

    /**
     * @param mixed $activLib
     */
    public function setActivLib($activLib): void
    {
        $this->activLib = $activLib;
    }

    /**
     * @return mixed
     */
    public function getActivDes()
    {
        return $this->activDes;
    }

    /**
     * @param mixed $activDes
     */
    public function setActivDes($activDes): void
    {
        $this->activDes = $activDes;
    }

    /**
     * @return mixed
     */
    public function getActivActCod()
    {
        return $this->activActCod;
    }

    /**
     * @param mixed $activActCod
     */
    public function setActivActCod($activActCod): void
    {
        $this->activActCod = $activActCod;
    }

    /**
     * @return mixed
     */
    public function getActivUserSai()
    {
        return $this->activUserSai;
    }

    /**
     * @param mixed $activUserSai
     */
    public function setActivUserSai($activUserSai): void
    {
        $this->activUserSai = $activUserSai;
    }

    /**
     * @return mixed
     */
    public function getActivUserModif()
    {
        return $this->activUserModif;
    }

    /**
     * @param mixed $activUserModif
     */
    public function setActivUserModif($activUserModif): void
    {
        $this->activUserModif = $activUserModif;
    }

    /**
     * @return mixed
     */
    public function getActivDatSai()
    {
        return $this->activDatSai;
    }

    /**
     * @param mixed $activDatSai
     */
    public function setActivDatSai($activDatSai): void
    {
        $this->activDatSai = $activDatSai;
    }

    /**
     * @return mixed
     */
    public function getActivDatModif()
    {
        return $this->activDatModif;
    }

    /**
     * @param mixed $activDatModif
     */
    public function setActivDatModif($activDatModif): void
    {
        $this->activDatModif = $activDatModif;
    }

    /**
     * @return mixed
     */
    public function getActivCacher()
    {
        return $this->activCacher;
    }

    /**
     * @param mixed $activCacher
     */
    public function setActivCacher($activCacher): void
    {
        $this->activCacher = $activCacher;
    }



}
