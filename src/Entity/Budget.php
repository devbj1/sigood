<?php

namespace App\Entity;

use App\Repository\BudgetRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BudgetRepository::class)
 */
class Budget
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="bud_mont", type="float")
     */
    private $budMont;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Exercice", cascade={"persist"})
     * @ORM\JoinColumn(name="Exercice", referencedColumnName="exe_num")
     */
    private $budExercice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Activite", cascade={"persist"})
     * @ORM\JoinColumn(name="Activite", referencedColumnName="activ_cod")
     */
    private $budActivite;

    /**
     * @var string
     *
     * @ORM\Column(name="bud_stat_cod", type="string")
     */
    private $budStatCod;

    /**
     * @var string
     *
     * @ORM\Column(name="bud_user_sai", type="string", length=500, nullable=true)
     */
    private $budUserSai;

    /**
     * @var string
     *
     * @ORM\Column(name="bud_user_val", type="string", length=500, nullable=true)
     */
    private $budUserVal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bud_dat_sai", type="datetime", nullable=true)
     */
    private $budDatSai;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bud_dat_val", type="datetime", nullable=true)
     */
    private $budDatVal;


    public function __construct()
    {
        $this->budStatCod = "BSN";
        $this->budDatSai = new \DateTime();

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getBudMont(): float
    {
        return $this->budMont;
    }

    /**
     * @param float $budMont
     */
    public function setBudMont(float $budMont): void
    {
        $this->budMont = $budMont;
    }

    /**
     * @return mixed
     */
    public function getBudExercice()
    {
        return $this->budExercice;
    }

    /**
     * @param mixed $budExercice
     */
    public function setBudExercice($budExercice): void
    {
        $this->budExercice = $budExercice;
    }

    /**
     * @return mixed
     */
    public function getBudActivite()
    {
        return $this->budActivite;
    }

    /**
     * @param mixed $budActivite
     */
    public function setBudActivite($budActivite): void
    {
        $this->budActivite = $budActivite;
    }

    /**
     * @return string
     */
    public function getBudStatCod(): string
    {
        return $this->budStatCod;
    }

    /**
     * @param string $budStatCod
     */
    public function setBudStatCod(string $budStatCod): void
    {
        $this->budStatCod = $budStatCod;
    }

    /**
     * @return string
     */
    public function getBudUserSai(): string
    {
        return $this->budUserSai;
    }

    /**
     * @param string $budUserSai
     */
    public function setBudUserSai(string $budUserSai): void
    {
        $this->budUserSai = $budUserSai;
    }

    /**
     * @return string
     */
    public function getBudUserVal(): string
    {
        return $this->budUserVal;
    }

    /**
     * @param string $budUserVal
     */
    public function setBudUserVal(string $budUserVal): void
    {
        $this->budUserVal = $budUserVal;
    }

    /**
     * @return \DateTime
     */
    public function getBudDatSai(): \DateTime
    {
        return $this->budDatSai;
    }

    /**
     * @param \DateTime $budDatSai
     */
    public function setBudDatSai(\DateTime $budDatSai): void
    {
        $this->budDatSai = $budDatSai;
    }

    /**
     * @return \DateTime
     */
    public function getBudDatVal(): \DateTime
    {
        return $this->budDatVal;
    }

    /**
     * @param \DateTime $budDatVal
     */
    public function setBudDatVal(\DateTime $budDatVal): void
    {
        $this->budDatVal = $budDatVal;
    }





}
