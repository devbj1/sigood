<?php

namespace App\Entity;

use App\Repository\TrimestreRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TrimestreRepository::class)
 */
class Trimestre
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=10)
     */
    private $trimCod;

    /**
     *
     * @ORM\Column(type="string", length=100)
     */
    private $trimLib;

    /**
     * @return mixed
     */
    public function getTrimCod()
    {
        return $this->trimCod;
    }

    /**
     * @param mixed $trimCod
     */
    public function setTrimCod($trimCod): void
    {
        $this->trimCod = $trimCod;
    }

    /**
     * @return mixed
     */
    public function getTrimLib()
    {
        return $this->trimLib;
    }

    /**
     * @param mixed $trimLib
     */
    public function setTrimLib($trimLib): void
    {
        $this->trimLib = $trimLib;
    }



}
