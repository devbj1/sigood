<?php

namespace App\Entity;

use App\Repository\AnalyseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnalyseRepository::class)
 */
class Analyse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Exercice", cascade={"persist"})
     * @ORM\JoinColumn(name="Exercice", referencedColumnName="exe_num")
     */
    private $analExercice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Activite", cascade={"persist"})
     * @ORM\JoinColumn(name="Activite", referencedColumnName="activ_cod")
     */
    private $analActivite;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Trimestre", cascade={"persist"})
     * @ORM\JoinColumn(name="Trimestre", referencedColumnName="trim_cod")
     */
    private $analTrimestre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cible", cascade={"persist"})
     * @ORM\JoinColumn(name="Cible", referencedColumnName="cib_cod")
     */
    private $analCible;

    /**
     * @var string
     *
     * @ORM\Column(name="anal_user_sai", type="string", length=500, nullable=true)
     */
    private $analUserSai;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="anal_dat_sai", type="datetime", nullable=true)
     */
    private $analDatSai;


    public function __construct()
    {
//        $this->analSerieChaine = "AN".$this->analExercice."".$this->analTrimestre."".;
        $this->analDatSai = new \DateTime();

    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAnalExercice()
    {
        return $this->analExercice;
    }

    /**
     * @param mixed $analExercice
     */
    public function setAnalExercice($analExercice): void
    {
        $this->analExercice = $analExercice;
    }

    /**
     * @return mixed
     */
    public function getAnalActivite()
    {
        return $this->analActivite;
    }

    /**
     * @param mixed $analActivite
     */
    public function setAnalActivite($analActivite): void
    {
        $this->analActivite = $analActivite;
    }

    /**
     * @return mixed
     */
    public function getAnalTrimestre()
    {
        return $this->analTrimestre;
    }

    /**
     * @param mixed $analTrimestre
     */
    public function setAnalTrimestre($analTrimestre): void
    {
        $this->analTrimestre = $analTrimestre;
    }

    /**
     * @return mixed
     */
    public function getAnalCible()
    {
        return $this->analCible;
    }

    /**
     * @param mixed $analCible
     */
    public function setAnalCible($analCible): void
    {
        $this->analCible = $analCible;
    }

    /**
     * @return string
     */
    public function getAnalUserSai(): string
    {
        return $this->analUserSai;
    }

    /**
     * @param string $analUserSai
     */
    public function setAnalUserSai(string $analUserSai): void
    {
        $this->analUserSai = $analUserSai;
    }

    /**
     * @return \DateTime
     */
    public function getAnalDatSai(): \DateTime
    {
        return $this->analDatSai;
    }

    /**
     * @param \DateTime $analDatSai
     */
    public function setAnalDatSai(\DateTime $analDatSai): void
    {
        $this->analDatSai = $analDatSai;
    }


}
