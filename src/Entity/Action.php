<?php

namespace App\Entity;

use App\Repository\ActionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ActionRepository::class)
 */
class Action
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=10)
     */
    private $actCod;

    /**
     *
     * @ORM\Column(type="string", length=500)
     */
    private $actLib;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable = true)
     */
    private $actDes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Programme", cascade={"persist"})
     * @ORM\JoinColumn(name="act_prog_cod", referencedColumnName="prog_cod")
     */
    private $actProgCod;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable = true)
     */
    private $actUserSai;

    /**
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $actDatSai;

    /**
     *
     * @ORM\Column(type="string", length=500, nullable = true)
     */
    private $actUserModif;

    /**
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $actDateModif;

    /**
     *
     * @ORM\Column(type="boolean", nullable = true)
     */
    private $actCacher;


    public function __construct()
    {
        $this->actCacher = false;
        $this->actDatSai = new \DateTime();

    }

    /**
     * @return mixed
     */
    public function getActCod()
    {
        return $this->actCod;
    }

    /**
     * @param mixed $actCod
     */
    public function setActCod($actCod): void
    {
        $this->actCod = $actCod;
    }

    /**
     * @return mixed
     */
    public function getActLib()
    {
        return $this->actLib;
    }

    /**
     * @param mixed $actLib
     */
    public function setActLib($actLib): void
    {
        $this->actLib = $actLib;
    }

    /**
     * @return mixed
     */
    public function getActDes()
    {
        return $this->actDes;
    }

    /**
     * @param mixed $actDes
     */
    public function setActDes($actDes): void
    {
        $this->actDes = $actDes;
    }

    /**
     * @return mixed
     */
    public function getActProgCod()
    {
        return $this->actProgCod;
    }

    /**
     * @param mixed $actProgCod
     */
    public function setActProgCod($actProgCod): void
    {
        $this->actProgCod = $actProgCod;
    }

    /**
     * @return mixed
     */
    public function getActUserSai()
    {
        return $this->actUserSai;
    }

    /**
     * @param mixed $actUserSai
     */
    public function setActUserSai($actUserSai): void
    {
        $this->actUserSai = $actUserSai;
    }

    /**
     * @return mixed
     */
    public function getActDatSai()
    {
        return $this->actDatSai;
    }

    /**
     * @param mixed $actDatSai
     */
    public function setActDatSai($actDatSai): void
    {
        $this->actDatSai = $actDatSai;
    }

    /**
     * @return mixed
     */
    public function getActUserModif()
    {
        return $this->actUserModif;
    }

    /**
     * @param mixed $actUserModif
     */
    public function setActUserModif($actUserModif): void
    {
        $this->actUserModif = $actUserModif;
    }

    /**
     * @return mixed
     */
    public function getActDateModif()
    {
        return $this->actDateModif;
    }

    /**
     * @param mixed $actDateModif
     */
    public function setActDateModif($actDateModif): void
    {
        $this->actDateModif = $actDateModif;
    }

    /**
     * @return mixed
     */
    public function getActCacher()
    {
        return $this->actCacher;
    }

    /**
     * @param mixed $actCacher
     */
    public function setActCacher($actCacher): void
    {
        $this->actCacher = $actCacher;
    }

}
