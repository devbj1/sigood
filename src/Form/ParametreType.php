<?php

namespace App\Form;

use App\Entity\Parametre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParametreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('paramCod', TextType::class, [
                'label' => 'Code',
            ])
            ->add('paramLibelle', TextType::class, [
                'label' => 'Libelle',
            ])
            ->add('paramDateEffet', DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'label' => 'Date effet',
                'required' => false,
            ])
            ->add('paramValeurInt', NumberType::class, [
                'label' => 'Valeur Entier',
            ])
            ->add('paramValeurString', TextType::class, [
                'label' => 'Valeur Chaine',
            ])
            ->add('paramValeurImage', TextType::class, [
                'label' => 'Valeur Image',
            ])
            ->add('paramModifiable', CheckboxType::class, [
                'label' => 'Modifiable ?',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Parametre::class,
        ]);
    }
}
