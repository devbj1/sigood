<?php

namespace App\Form;

use App\Entity\Ministere;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MinistereType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('minCod', TextType::class, [
                'label' => 'Code',
            ])->add('minLibCrt', TextType::class, [
                'label' => 'Libelle Court',
            ])->add('minLibLong', TextType::class, [
                'label' => 'Libelle Long',
            ])->add('minAdres', TextType::class, [
                'label' => 'Adresse',
                'required' => false,
            ])->add('minTel', TextType::class, [
                'label' => 'Téléphone',
                'required' => false,
            ])->add('minEmail', TextType::class, [
                'label' => 'Email',
                'required' => false,
            ])->add('minSite', TextType::class, [
                'label' => 'Site',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ministere::class,
        ]);
    }
}
