<?php

namespace App\Form;

use App\Entity\Activite;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActiviteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('activLib',TextType::class,[
                'label' => 'Libelle',
            ])
            ->add('activDes', TextareaType::class, [
                'label' => 'Description',
                'attr' => ['rows' => 7],
                'required' => false,
            ])
//            ->add('activUserSai')->add('activUserModif')->add('activDatSai')->add('activDatModif')->add('activCacher')
            ->add('activActCod',EntityType::class,[
                'choice_label' => 'actLib',
                'class' => "App\Entity\Action",
                'label' => 'Action',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Activite::class,
        ]);
    }
}
