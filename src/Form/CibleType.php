<?php

namespace App\Form;

use App\Entity\Cible;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CibleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cibCod', TextType::class, [
                'label' => 'Code',
            ])
            ->add('cibLib', TextType::class, [
                'label' => 'Libelle',
            ])
//            ->add('cibUserSai')->add('cibUserModif')->add('cibDatSai')->add('cibDatModif')->add('cibCacher')
            ->add('cibOddCod', EntityType::class, [
                'choice_label' => 'oddLib',
                'class' => "App\Entity\Odd",
                'label' => 'Odd',
            ])
            ->add('cibCibTyp', EntityType::class, [
                'choice_label' => 'typCibLib',
                'class' => "App\Entity\TypeCible",
                'label' => 'Type Cible',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cible::class,
        ]);
    }
}
