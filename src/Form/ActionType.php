<?php

namespace App\Form;

use App\Entity\Action;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('actLib', TextType::class, [
                'label' => 'Libelle',
            ])
            ->add('actDes', TextareaType::class, [
                'label' => 'Description',
                'attr' => ['rows' => 7],
                'required' => false,
            ])
//            ->add('actUserSai')->add('actDatSai')->add('actUserModif')->add('actDateModif')->add('actCacher')
            ->add('actProgCod', EntityType::class, [
                'choice_label' => 'progLib',
                'class' => "App\Entity\Programme",
                'label' => 'Programme',
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Action::class,
        ]);
    }
}
