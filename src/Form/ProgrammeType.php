<?php

namespace App\Form;

use App\Entity\Programme;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProgrammeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('progLib', TextType::class, [
                'label' => 'Libelle',
            ])
            ->add('progDes', TextareaType::class, [
                'label' => 'Description',
                'attr' => ['rows' => 7],
                'required' => false,
            ])
            ->add('progMinCod', EntityType::class, [
                'choice_label' => 'minLibLong',
                'class' => "App\Entity\Ministere",
                'label' => 'Ministere',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Programme::class,
        ]);
    }
}
