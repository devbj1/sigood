<?php

namespace App\Controller;

use App\Entity\TypeCible;
use App\Form\TypeCibleType;
use App\Repository\TypeCibleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/type/cible")
 */
class TypeCibleController extends AbstractController
{
    /**
     * @Route("/", name="type_cible_index", methods={"GET"})
     */
    public function index(TypeCibleRepository $typeCibleRepository): Response
    {
        return $this->render('type_cible/index.html.twig', [
            'type_cibles' => $typeCibleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="type_cible_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typeCible = new TypeCible();
        $form = $this->createForm(TypeCibleType::class, $typeCible);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typeCible);
            $entityManager->flush();

            return $this->redirectToRoute('type_cible_index');
        }

        return $this->render('type_cible/new.html.twig', [
            'type_cible' => $typeCible,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{tyCibCod}", name="type_cible_show", methods={"GET"})
     */
    public function show(TypeCible $typeCible): Response
    {
        return $this->render('type_cible/show.html.twig', [
            'type_cible' => $typeCible,
        ]);
    }

    /**
     * @Route("/{tyCibCod}/edit", name="type_cible_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypeCible $typeCible): Response
    {
        $form = $this->createForm(TypeCibleType::class, $typeCible);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('type_cible_index');
        }

        return $this->render('type_cible/edit.html.twig', [
            'type_cible' => $typeCible,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{tyCibCod}", name="type_cible_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TypeCible $typeCible): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeCible->getTyCibCod(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typeCible);
            $entityManager->flush();
        }

        return $this->redirectToRoute('type_cible_index');
    }
}
