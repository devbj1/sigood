<?php

namespace App\Controller;

use App\Entity\Exercice;
use App\Form\ExerciceType;
use App\Repository\ExerciceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/exercice")
 */
class ExerciceController extends AbstractController
{
    /**
     * @Route("/", name="exercice_index", methods={"GET"})
     */
    public function index(ExerciceRepository $exerciceRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('exercice/index.html.twig', [
            'exercices' => $exerciceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="exercice_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $exercice = new Exercice();
        $form = $this->createForm(ExerciceType::class, $exercice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();
            $entityManager = $this->getDoctrine()->getManager();
            $exercice->setExeUserSai($session->get('nom'). ' ' .$session->get('prenom'));
            $entityManager->persist($exercice);
            $entityManager->flush();

            $session = new Session();
            if ($exercice->getExeNum() == "") {
                $session->getFlashBag()->add('error', 'Erreur enregistrement !');
            } else {
                $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
            }

            return $this->redirectToRoute('exercice_index');
        }

        return $this->render('exercice/new.html.twig', [
            'exercice' => $exercice,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{exeNum}", name="exercice_show", methods={"GET"})
     */
    public function show(Exercice $exercice): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('exercice/show.html.twig', [
            'exercice' => $exercice,
        ]);
    }

    /**
     * @Route("/{exeNum}/edit", name="exercice_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Exercice $exercice): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(ExerciceType::class, $exercice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session  = new Session();

            $exercice->setExeUserModif($session->get('nom'). ' ' .$session->get('prenom'));
            $exercice->setExeDatModif(new \DateTime());
            $this->getDoctrine()->getManager()->flush();
            if ($exercice->getExeNum() == "") {
                $session->getFlashBag()->add('error', 'Erreur modification !');
            } else {
                $session->getFlashBag()->add('success', 'Modification effectuée avec succès !');
            }
            return $this->redirectToRoute('exercice_index');
        }

        return $this->render('exercice/edit.html.twig', [
            'exercice' => $exercice,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{exeNum}", name="exercice_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Exercice $exercice): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($this->isCsrfTokenValid('delete'.$exercice->getExeNum(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($exercice);
            $entityManager->flush();
        }

        return $this->redirectToRoute('exercice_index');
    }


    /**
     * @Route("/{exeNum}/active", name="exercice_active", methods={"GET"})
     */
    public function active(Request $request, Exercice $exercice): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $entityManager = $this->getDoctrine()->getManager();

        $updatestat = $entityManager->getRepository('App:Exercice')->updateStatutExercice(false);
//        dump($updatestat);die();

        if ($this->isCsrfTokenValid('active'.$exercice->getExeNum(), $request->request->get('_token'))) {
            $exercice->setExeEncours(true);
            $entityManager->persist($exercice);
            $entityManager->flush();
            $session = new Session();
            if ($exercice->getExeNum() == 0){
                $session->getFlashBag()->add('error', 'Erreur activation exercice !');
            }else{
                $session->getFlashBag()->add('success', 'Exercice activé avec succès !');
            }
        }

        return $this->redirectToRoute('exercice_index');
    }

    /**
     * @Route("/{exeNum}/desactive", name="exercice_desactive", methods={"GET"})
     */
    public function desactive(Request $request, Exercice $exercice): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($this->isCsrfTokenValid('desactive'.$exercice->getExeNum(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $exercice->setExeEncours(false);
            $entityManager->persist($exercice);
            $entityManager->flush();
            $session = new Session();

            if ($exercice->getExeNum() == 0){
                $session->getFlashBag()->add('error', 'Erreur désactivation exercice !');
            }else{
                $session->getFlashBag()->add('success', 'Exercice désactivation avec succès !');
            }
        }

        return $this->redirectToRoute('exercice_index');
    }
}
