<?php

namespace App\Controller;

use App\Entity\Exercice;
use App\Entity\Trimestre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $session = new Session();

        $em = $this->getDoctrine()->getManager();

        $nbrministere = count($em->getRepository('App:Ministere')->findAll());

        $montantaccorde = $em->getRepository('App:Execution')->sumActiviteExecutionByExerciceByMinistere($session->get('exercice'),"BSV",$session->get('ministere'))[0]['total'];

        $montantexecute = $em->getRepository('App:Execution')->sumExecutionByMinistere($session->get('ministere'),$session->get('exercice'))[0]['mont_exec'];

//        dump($session->get('ministere'),$session->get('exoid'));die();
//        dump($em->getRepository('App:Execution')->sumExecutionByExo('2021'));
//        dump($em->getRepository('App:Execution')->sumExecutionByExo('2020'));die();

//        dump($montantexecute);die();
//        $listesmontantparodd = $em->getRepository('App:Execution')->findMontantParOddByExerciceByMinistere($session->get('exercice'),"BSV",$session->get('ministere'));

//        $tabodd ="";
//        $taboddmontant ="";
////        dump($listesmontantparodd);die();
//        foreach ($listesmontantparodd as $listemontantparodd) {
//            if ($tabodd =="" ){
//                $tabodd = '\''.$listemontantparodd['oddCod'].'\'';
//                $taboddmontant = $listemontantparodd['montOdd'];
//            }else{
//                $tabodd = $tabodd.', \''.$listemontantparodd['oddCod'].'\'';
//                $taboddmontant = $taboddmontant.','.$listemontantparodd['montOdd'];
//            }
//
//        }

//        $tabodd = json_encode($tabodd);
//        $tabodd = json_encode($tabodd);
//        $taboddmontant = $taboddmontant;
//        dump($tabodd,$taboddmontant);die();
//        $montantexecute = count($em->getRepository('AppBundle:Ministere')->findAll());

        $montantdisponible = $montantaccorde-$montantexecute;

        // replace this example code with whatever you need
        return $this->render('dashbord/index.html.twig',compact('nbrministere','montantaccorde','montantexecute','montantdisponible'));
    }



    /**
     * @Route("/elaboration_budget/chargement", name="elabor_charg")
     */
    public function indexChargementElaborationAction(Request $request)
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // replace this example code with whatever you need
        return $this->render('programmation/elaboration_chargement.html.twig');
    }


    /**
     * @Route("/elaboration_budget/validation", name="elabor_vali")
     */
    public function indexValidationElaborationAction(Request $request)
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $session = new Session();

        $em = $this->getDoctrine()->getManager();

        $listesActivExo = $em->getRepository('App:Budget')->findActiviteBudgetByExercice($session->get('exercice'),"BSN");
//        dump($listesActivExo);die();
        // replace this example code with whatever you need
        return $this->render('validation/elaboration_validation.html.twig', compact('listesActivExo'));
    }


    /**
     * @Route("/analyse_budget/chargement", name="analys_charg")
     */
    public function indexChargementAnalyseAction(Request $request)
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $session = new Session();

        $em = $this->getDoctrine()->getManager();

        $listesActivExo = $em->getRepository('App:Analyse')->findActiviteAnalyseByExercice($session->get('exercice'));


        // replace this example code with whatever you need
        return $this->render('programmation/analyse_budget.html.twig', compact('listesActivExo'));
    }

    /**
     * @Route("/execution_budget", name="execution_budget")
     */
    public function indexExecutionBudgetAction(Request $request)
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $session = new Session();

        $em = $this->getDoctrine()->getManager();
//        dump($session->get('ministere'));die();
        $listesActivMinisExo = $em->getRepository('App:Execution')->findActiviteExecutionByExerciceByMinistere($session->get('exercice'),"BSV",$session->get('ministere'));

        $listesActivMinisExoMontExec = [];

        foreach ($listesActivMinisExo as $listeActivMinisExo) {
            $tab = [];
            $tab['id'] = $listeActivMinisExo['id'];
            $tab['cible'] = $listeActivMinisExo['cibCod'];
            $tab['activite'] = $listeActivMinisExo['activLib'];
            $tab['montant'] = $listeActivMinisExo['budMont'];
            $tab['montExecut'] = $em->getRepository('App:Execution')->sumExecutionByBudget($listeActivMinisExo['id'])[0]['mont_exec'];

            array_push($listesActivMinisExoMontExec, $tab);
        }


        $trimestres = $em->getRepository('App:Trimestre')->findAll();

//        dump($listesActivMinisExoMontExec);die();
        // replace this example code with whatever you need
        return $this->render('programmation/execution_budget.html.twig',compact('listesActivMinisExoMontExec','trimestres'));
    }

    /**
     * @Route("/imprimertest", name="imprimertest")
     */
    public function imprimertest()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $exercices = $entityManager->getRepository(Exercice::class)->findAll();
        $trimestres = $entityManager->getRepository(Trimestre::class)->findAll();

        $exercices = array_reverse($exercices);

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('statistiques/analyse_budgetaire_dun_ministere/pdf.html.twig');

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'paysage');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (inline view)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);

        exit(0);
    }


}
