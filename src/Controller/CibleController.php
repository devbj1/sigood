<?php

namespace App\Controller;

use App\Entity\Cible;
use App\Form\CibleType;
use App\Repository\CibleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cible")
 */
class CibleController extends AbstractController
{
    /**
     * @Route("/", name="cible_index", methods={"GET"})
     */
    public function index(CibleRepository $cibleRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('cible/index.html.twig', [
            'cibles' => $cibleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="cible_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $cible = new Cible();
        $form = $this->createForm(CibleType::class, $cible);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();
            $entityManager = $this->getDoctrine()->getManager();
            $cible->setCibUserSai($session->get('nom'). ' ' .$session->get('prenom'));
            $entityManager->persist($cible);
            $entityManager->flush();

            $session = new Session();
            if ($cible->getCibCod() == "") {
                $session->getFlashBag()->add('error', 'Erreur enregistrement !');
            } else {
                $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
            }


            return $this->redirectToRoute('cible_index');
        }

        return $this->render('cible/new.html.twig', [
            'cible' => $cible,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{cibCod}", name="cible_show", methods={"GET"})
     */
    public function show(Cible $cible): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('cible/show.html.twig', [
            'cible' => $cible,
        ]);
    }

    /**
     * @Route("/{cibCod}/edit", name="cible_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Cible $cible): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(CibleType::class, $cible);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session  = new Session();

            $cible->setCibUserModif($session->get('nom'). ' ' .$session->get('prenom'));
            $cible->setCibDatModif(new \DateTime());
            $this->getDoctrine()->getManager()->flush();
            if ($cible->getCibCod() == "") {
                $session->getFlashBag()->add('error', 'Erreur modification !');
            } else {
                $session->getFlashBag()->add('success', 'Modification effectuée avec succès !');
            }
            return $this->redirectToRoute('cible_index');
        }

        return $this->render('cible/edit.html.twig', [
            'cible' => $cible,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{cibCod}", name="cible_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Cible $cible): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($this->isCsrfTokenValid('delete'.$cible->getCibCod(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cible);
            $entityManager->flush();
        }

        return $this->redirectToRoute('cible_index');
    }
}
