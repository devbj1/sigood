<?php

namespace App\Controller;

use App\Entity\Ministere;
use App\Form\MinistereType;
use App\Repository\MinistereRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ministere")
 */
class MinistereController extends AbstractController
{
    /**
     * @Route("/", name="ministere_index", methods={"GET"})
     */
    public function index(MinistereRepository $ministereRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('ministere/index.html.twig', [
            'ministeres' => $ministereRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="ministere_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $ministere = new Ministere();
        $form = $this->createForm(MinistereType::class, $ministere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session  = new Session();
            $entityManager = $this->getDoctrine()->getManager();
            $ministere->setMinUserSai($session->get('nom'). ' ' .$session->get('prenom'));
            $entityManager->persist($ministere);
            $entityManager->flush();

            $session = new Session();
            if ($ministere->getMinCod() == "") {
                $session->getFlashBag()->add('error', 'Erreur enregistrement !');
            } else {
                $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
            }


            return $this->redirectToRoute('ministere_index');
        }

        return $this->render('ministere/new.html.twig', [
            'ministere' => $ministere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{minCod}", name="ministere_show", methods={"GET"})
     */
    public function show(Ministere $ministere): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('ministere/show.html.twig', [
            'ministere' => $ministere,
        ]);
    }

    /**
     * @Route("/{minCod}/edit", name="ministere_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Ministere $ministere): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(MinistereType::class, $ministere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session  = new Session();

            $ministere->setMinUserModif($session->get('nom'). ' ' .$session->get('prenom'));
            $ministere->setMinDatModif(new \DateTime());
            $this->getDoctrine()->getManager()->flush();
            if ($ministere->getMinCod() == "") {
                $session->getFlashBag()->add('error', 'Erreur modification !');
            } else {
                $session->getFlashBag()->add('success', 'Modification effectuée avec succès !');
            }

            return $this->redirectToRoute('ministere_index');
        }

        return $this->render('ministere/edit.html.twig', [
            'ministere' => $ministere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{minCod}", name="ministere_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Ministere $ministere): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($this->isCsrfTokenValid('delete'.$ministere->getMinCod(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ministere);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ministere_index');
    }
}
