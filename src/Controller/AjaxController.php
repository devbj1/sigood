<?php

namespace App\Controller;

use App\Entity\Analyse;
use App\Entity\Budget;
use App\Entity\Execution;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\StreamedResponse;

class AjaxController extends Controller
{


    public function sessionVariable($exo, $min, $tab1, $tab2, $tab3)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $session = new Session();

        $session->set("exopdf", $exo);

        $session->set("ministerepdf", $min);

        $session->set("tableau1", $tab1);

        $session->set("tableau2", $tab2);

        $session->set("tableau3", $tab3);

//        dump($session);die();
//        return new JsonResponse(array('statut' => 'succes','contenu' => "",'dataRep' => $dataRep));

    }

    /**
     * @Route("/detailBudgetExecute", name="ajax_detail_budget_execute")
     */
    public function detailBudgetExecuteAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // retrieve the file with the name given in the form.
        // do var_dump($request->files->all()); if you need to know if the file is being uploaded.

        $em = $this->getDoctrine()->getManager();

        $exercice = $request->get('exe');

        $ministeres = $em->getRepository('App:Budget')->findMinistereByExercice($exercice, "BSV");

        $dataDetBud = [];

//        dump($ministeres);die();
//Ministere/**/
        foreach ($ministeres as $ministere) {

            $ministeremontantexecute = $em->getRepository('App:Execution')->sumExecutionByMinistere($ministere['minCod'],$exercice);
//            dump($ministeremontantexecute);die();
            $tab1['code'] = $ministere['minCod'];
            $tab1['libelle'] = $ministere['minLibLong'];
            $tab1['montantplanifie'] = $ministere['total'];
            $tab1['montantexecute'] = $ministeremontantexecute[0]['mont_exec'];
            $tab1['montantdisponible'] = $ministere['total']-$ministeremontantexecute[0]['mont_exec'];

            if ($ministeremontantexecute[0]['mont_exec'] == 0){
                $tab1['pourcentage'] = 0;
            }else{
                $tab1['pourcentage'] = number_format($ministeremontantexecute[0]['mont_exec']/$ministere['total'], 2);
            }

            $tab1['type'] = 'min';

            array_push($dataDetBud, $tab1);
//Programme
            $listesprogrammeministere = $em->getRepository('App:Budget')->findProgrammeMinistereByExercice($exercice, "BSV", $ministere['minCod']);

            foreach ($listesprogrammeministere as $programmeministere) {

                $programmemontantexecute = $em->getRepository('App:Execution')->sumExecutionByMinistereByProgramme($exercice,$programmeministere['progCod'], $ministere['minCod']);

                $tab1['code'] = $programmeministere['progCod'];
                $tab1['libelle'] = $programmeministere['progLib'];
                $tab1['montantplanifie'] = $programmeministere['total'];
                $tab1['montantexecute'] = $programmemontantexecute[0]['mont_exec'];
                $tab1['montantdisponible'] = $programmeministere['total']-$programmemontantexecute[0]['mont_exec'];

                if ($programmemontantexecute[0]['mont_exec'] == 0){
                    $tab1['pourcentage'] = 0;
                }else{
                    $tab1['pourcentage'] = number_format($programmemontantexecute[0]['mont_exec']/$programmeministere['total'], 2);
                }

                $tab1['type'] = 'prog';

//                dump($tab);die();
                array_push($dataDetBud, $tab1);
//Action
                $listesactionprogrammeministere = $em->getRepository('App:Budget')->findActionProgrammeMinistereByExercice($exercice, "BSV", $ministere['minCod'], $programmeministere['progCod']);

                foreach ($listesactionprogrammeministere as $actionprogrammeministere) {

                    $actionmontantexecute = $em->getRepository('App:Execution')->sumExecutionByMinistereByAction($exercice,$actionprogrammeministere['actCod'], $ministere['minCod']);

                    $tab1['code'] = $actionprogrammeministere['actCod'];
                    $tab1['libelle'] = $actionprogrammeministere['actLib'];
                    $tab1['montantplanifie'] = $actionprogrammeministere['total'];
                    $tab1['montantexecute'] = $actionmontantexecute[0]['mont_exec'];
                    $tab1['montantdisponible'] = $actionprogrammeministere['total']-$actionmontantexecute[0]['mont_exec'];


                    if ($actionmontantexecute[0]['mont_exec'] == 0){
                        $tab1['pourcentage'] = 0;
                    }else{
                        $tab1['pourcentage'] = number_format($actionmontantexecute[0]['mont_exec']/$actionprogrammeministere['total'], 2);

//                        $tab1['pourcentage'] = $actionprogrammeministere['total']/$actionmontantexecute[0]['mont_exec'];
                    }
                    $tab1['type'] = 'act';

//                dump($actionprogrammeministere);die();
                    array_push($dataDetBud, $tab1);

//Activite
                    $listesactiviteactionprogrammeministere = $em->getRepository('App:Budget')->findActiviteActionProgrammeMinistereByExercice($exercice, "BSV", $ministere['minCod'], $programmeministere['progCod'],$actionprogrammeministere['actCod']);

                    foreach ($listesactiviteactionprogrammeministere as $activiteactionprogrammeministere) {

                        $activitemontantexecute = $em->getRepository('App:Execution')->sumExecutionByMinistereByActivite($exercice,$activiteactionprogrammeministere['activCod'], $ministere['minCod']);

                        $tab1['code'] = $activiteactionprogrammeministere['activCod'];
                        $tab1['libelle'] = $activiteactionprogrammeministere['activLib'];
                        $tab1['montantplanifie'] = $activiteactionprogrammeministere['total'];
                        $tab1['montantexecute'] = $activitemontantexecute[0]['mont_exec'];
                        $tab1['montantdisponible'] = $activiteactionprogrammeministere['total']-$activitemontantexecute[0]['mont_exec'];

                        if ($activitemontantexecute[0]['mont_exec'] == 0){
                            $tab1['pourcentage'] = 0;
                        }else{
                            $tab1['pourcentage'] = number_format($activitemontantexecute[0]['mont_exec']/$activiteactionprogrammeministere['total'], 2);
//                            $tab1['pourcentage'] = $activiteactionprogrammeministere['total']/$activitemontantexecute[0]['mont_exec'];
                        }

                        $tab1['type'] = 'activ';

//                dump($tab);die();
                        array_push($dataDetBud, $tab1);
                    }
                }
            }

        }

        $this->sessionVariable($exercice, "", $dataDetBud, "", "");

//        dump($dataDetBud);die();

        return new JsonResponse(array('statut' => 'succes', 'contenu' => "", 'dataDetBud' => $dataDetBud));

    }


    /**
     * @Route("/detailBudgetPlanifie", name="ajax_detail_budget_planifie")
     */
    public function detailBudgetPlanifieAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // retrieve the file with the name given in the form.
        // do var_dump($request->files->all()); if you need to know if the file is being uploaded.

        $em = $this->getDoctrine()->getManager();

        $exercice = $request->get('exe');

        $ministeres = $em->getRepository('App:Budget')->findMinistereByExercice($exercice, "BSV");

        $dataDetBud = [];

//        dump($ministeres);die();
//Ministere
        foreach ($ministeres as $ministere) {

            $tab1['code'] = $ministere['minCod'];
            $tab1['libelle'] = $ministere['minLibLong'];
            $tab1['montant'] = $ministere['total'];
            $tab1['type'] = 'min';

            array_push($dataDetBud, $tab1);
//Programme
            $listesprogrammeministere = $em->getRepository('App:Budget')->findProgrammeMinistereByExercice($exercice, "BSV", $ministere['minCod']);

            foreach ($listesprogrammeministere as $programmeministere) {
                $tab1['code'] = $programmeministere['progCod'];
                $tab1['libelle'] = $programmeministere['progLib'];
                $tab1['montant'] = $programmeministere['total'];
                $tab1['type'] = 'prog';

//                dump($tab);die();
                array_push($dataDetBud, $tab1);
//Action
                $listesactionprogrammeministere = $em->getRepository('App:Budget')->findActionProgrammeMinistereByExercice($exercice, "BSV", $ministere['minCod'], $programmeministere['progCod']);

                foreach ($listesactionprogrammeministere as $actionprogrammeministere) {
                    $tab1['code'] = $actionprogrammeministere['actCod'];
                    $tab1['libelle'] = $actionprogrammeministere['actLib'];
                    $tab1['montant'] = $actionprogrammeministere['total'];
                    $tab1['type'] = 'act';

//                dump($actionprogrammeministere);die();
                    array_push($dataDetBud, $tab1);

//Activite
                    $listesactiviteactionprogrammeministere = $em->getRepository('App:Budget')->findActiviteActionProgrammeMinistereByExercice($exercice, "BSV", $ministere['minCod'], $programmeministere['progCod'],$actionprogrammeministere['actCod']);

                    foreach ($listesactiviteactionprogrammeministere as $activiteactionprogrammeministere) {
                        $tab1['code'] = $activiteactionprogrammeministere['activCod'];
                        $tab1['libelle'] = $activiteactionprogrammeministere['activLib'];
                        $tab1['montant'] = $activiteactionprogrammeministere['total'];
                        $tab1['type'] = 'activ';

//                dump($tab);die();
                        array_push($dataDetBud, $tab1);
                    }
                }
            }

        }

        $this->sessionVariable($exercice, "", $dataDetBud, "", "");

//        dump($dataDetBud);die();

        return new JsonResponse(array('statut' => 'succes', 'contenu' => "", 'dataDetBud' => $dataDetBud));

    }


    /**
     * @Route("/repartitionBudgetaireMinistere", name="ajax_repartition_budgetaire_ministere")
     */
    public function repartitionBudgetaireMinistereAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // retrieve the file with the name given in the form.
        // do var_dump($request->files->all()); if you need to know if the file is being uploaded.

        $em = $this->getDoctrine()->getManager();

        $exercice = $request->get('exe');

        $listesmontantaccorde = $em->getRepository('App:Execution')->findActiviteExecutionByExerciceByMinistereByAnalyseParMinistere($exercice, "BSV");

        $dataRep = [];

        foreach ($listesmontantaccorde as $listemontantaccorde) {

            $tab1['label'] = $listemontantaccorde['minLibCrt'];
            $tab1['data'] = $listemontantaccorde['total'];

//                dump($tab);die();
            array_push($dataRep, $tab1);
        }

        return new JsonResponse(array('statut' => 'succes', 'contenu' => "", 'dataRep' => $dataRep));

    }


    /**
     * @Route("/repartitionDetailleBudgetaireMinistere", name="ajax_repartition_detaille_budgetaire_ministere")
     */
    public function repartitionDetailleBudgetaireMinistereAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // retrieve the file with the name given in the form.
        // do var_dump($request->files->all()); if you need to know if the file is being uploaded.

        $em = $this->getDoctrine()->getManager();

        $exercice = $request->get('exe');

//        dump($em->getRepository('App:Execution')->findActiviteExecutionByExerciceByMinistereByAnalyseParMinistere($exercice,"BSV"));die();

        $listesmontantaccorde = $em->getRepository('App:Execution')->findActiviteExecutionByExerciceByMinistereByAnalyseParMinistere($exercice, "BSV");

        $montantciblemo = $em->getRepository('App:Execution')->sumMontantParTypeCibleByExercice($exercice, "BSV", "MEO")[0]['montTypeCible'];

        $montantaccord = $em->getRepository('App:Execution')->sumActiviteExecutionByExerciceAllMinistere($exercice, "BSV")[0]['total'];

//        $montantaccorde = $em->getRepository('App:Execution')->sumActiviteExecutionByExerciceByMinistere($exercice, "BSV", $ministere)[0]['total'];


        $listes = [];
        $donutDataCible = [];
//        dump($listesmontantaccorde);die();
        //MISE EN OEUVRE
//        dump($montantciblemo);die();
        $montanttotalpartypeciblemo = $em->getRepository('App:Execution')->sumMontantParOddByExercice($exercice, "BSV")[0]['montOdd'];

        $montantpartypeciblemo = 0;

        foreach ($listesmontantaccorde as $listemontantaccorde) {

//                $listesmontantparcibleodd = $em->getRepository('AppBundle:Execution')->findMontantParCibleOddByExerciceByMinistere($session->get('exoid'),"BSV",$session->get('codeministere'),$listemontantparodd['id']);
//            dump($listemontantaccorde['minCod']);
            $tab['Ministere'] = $listemontantaccorde['minLibLong'];

//            $montantpartypeciblemo = $em->getRepository('App:Execution')->findMontantParTypeCibleByExerciceByMinistere($exercice, "BSV", $listemontantaccorde['minCod'], "MEO");
//
//            if ($montantpartypeciblemo === []) {
//
//                $montantpartypeciblemo = 0;
//            } else {
//                $montantpartypeciblemo = $montantpartypeciblemo[0]['montTypeCible'];
//            }

            $montantpartypeciblemo = $montantpartypeciblemo + $em->getRepository('App:Execution')->sumMontantParOddByExerciceByMinistere($exercice, "BSV", $listemontantaccorde['minCod'])[0]['montOdd'];

//            dump($montantpartypeciblemo,$montantaccord);die();
            $tab['BudMin'] = $listemontantaccorde['total'];
            $tab['MontantCible'] = number_format($montantpartypeciblemo, 2);
            $tab['PartOddDepense'] = number_format(($montantpartypeciblemo / $montanttotalpartypeciblemo) * 100, 2);
            $tab['PartOddBudget'] = number_format(($montantpartypeciblemo / $montantaccord) * 100, 2);
            $tab['type'] = 'ministere';

            $tab1['label'] = $listemontantaccorde['minLibCrt'];
            $tab1['data'] = $montantpartypeciblemo;
//                $tab1['color'] = '#0073b7';

//                dump($tab);die();
            array_push($listes, $tab);
            array_push($donutDataCible, $tab1);
        }
//        dump($listes,$donutDataCible);die();

        $tab['Ministere'] = "Total des ODD";
        $tab['BudMin'] = $montantpartypeciblemo;
        $tab['MontantCible'] = "";
        $tab['PartOddDepense'] = number_format(($montantpartypeciblemo/$montanttotalpartypeciblemo) * 100, 2);
        $tab['PartOddBudget'] = number_format(($montantpartypeciblemo/$montantaccord) * 100, 2);
        $tab['type'] = 'tdcodd';
        array_push($listes, $tab);

        $tab['Ministere'] = "BUDGET TOTAL";
        $tab['BudMin'] = $montantaccord;
        $tab['MontantCible'] = "";
        $tab['PartOddDepense'] = "";
        $tab['PartOddBudget'] = "";
        $tab['type'] = 'budtot';
        array_push($listes, $tab);

//        dump($listes,$donutDataCible);die();

        $this->sessionVariable($exercice, "", $listes, "", "");


        return new JsonResponse(array('statut' => 'succes', 'contenu' => $listes, 'dataCible' => $donutDataCible));

    }


    /**
     * @Route("/analyseBudgetairePeriodeOdd", name="ajax_analyse_budgetaire_periode_odd")
     */
    public function analyseBudgetairePeriodeOddAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        // retrieve the file with the name given in the form.
        // do var_dump($request->files->all()); if you need to know if the file is being uploaded.
        $session = new Session();

        $em = $this->getDoctrine()->getManager();

        $exercice = $request->get('exe');

        $trimestre = $request->get('trimestre');

        $ministere = $request->get('min');

//        $montantaccorde = $em->getRepository('App:Execution')->sumActiviteExecutionByExerciceByMinistereByAnalyseByTrimestre($exercice,"BSV",$ministere,$trimestre)[0]['total'];
        $montantaccorde = $em->getRepository('App:Execution')->sumActiviteExecutionByExerciceByMinistere($exercice, "BSV", $ministere)[0]['total'];

//        $montantexecute = $em->getRepository('AppBundle:Execution')->sumExecutionByMinistere($session->get('codeministere'),$session->get('exoid'))[0]['mont_exec'];

        $listesmontantparodd = $em->getRepository('App:Execution')->findMontantParOddByExerciceByMinistereByTrimestre($exercice, "BSV", $ministere, $trimestre);

        //HORS MISE EN OEUVRE
        $montantpartypeciblehmo = $em->getRepository('App:Execution')->findMontantParTypeCibleByExerciceByMinistereByTrimestre($exercice, "BSV", $ministere, "HMEO", $trimestre)[0]['montTypeCible'];

        //MISE EN OEUVRE
//        $montantpartypeciblemo = $em->getRepository('App:Execution')->findMontantParTypeCibleByExerciceByMinistereByTrimestre($exercice,"BSV",$ministere,"MEO",$trimestre)[0]['montTypeCible'];
        $montantpartypeciblemo = $em->getRepository('App:Execution')->sumMontantParOddByExerciceByMinistereByTrimestre($exercice, "BSV", $ministere, $trimestre)[0]['montOdd'];

        $montanttotalcible = $montantpartypeciblemo;
//        $montanttotalcible = $montantpartypeciblehmo + $montantpartypeciblemo;

        $listes = [];
        $donutDataOdd = [];
        $donutDataCible = [];

        foreach ($listesmontantparodd as $listemontantparodd) {

            $listesmontantparcibleodd = $em->getRepository('App:Execution')->findMontantParCibleOddByExerciceByMinistereByTrimestre($exercice, "BSV", $ministere, $listemontantparodd['oddCod'], $trimestre);

            foreach ($listesmontantparcibleodd as $listemontantparcibleodd) {

//                $listesmontantparcibleodd = $em->getRepository('AppBundle:Execution')->findMontantParCibleOddByExerciceByMinistere($session->get('exoid'),"BSV",$session->get('codeministere'),$listemontantparodd['id']);
                $tab['ODD'] = $listemontantparcibleodd['cibCod'];
                $tab['Montant'] = $listemontantparcibleodd['montCible'];
                $tab['PartCible'] = number_format(($listemontantparcibleodd['montCible'] / $montantaccorde) * 100, 2);
                $tab['PartOdd'] = number_format(($listemontantparcibleodd['montCible'] / $montanttotalcible) * 100, 2);
                $tab['type'] = 'cible';

                $tab1['label'] = $listemontantparcibleodd['cibCod'];
                $tab1['data'] = $listemontantparcibleodd['montCible'];
//                $tab1['color'] = '#0073b7';

//                dump($listesmontantparcibleodd);
                array_push($listes, $tab);
                array_push($donutDataCible, $tab1);
            }
            $tab['ODD'] = "Total " . $listemontantparodd['oddCod'];
            $tab['Montant'] = $listemontantparodd['montOdd'];
            $tab['PartCible'] = number_format(($listemontantparodd['montOdd'] / $montantaccorde) * 100, 2);
            $tab['PartOdd'] = number_format(($listemontantparodd['montOdd'] / $montanttotalcible) * 100, 2);
            $tab['type'] = 'odd';

            $tab1['label'] = $listemontantparodd['oddCod'];
            $tab1['data'] = $listemontantparodd['montOdd'];
//            $tab1['color'] = '#00c0ef';
            array_push($listes, $tab);

            array_push($donutDataOdd, $tab1);

        }
        $tab['ODD'] = "Total Hors Mise en Oeuvre";
        $tab['Montant'] = $montantpartypeciblehmo;
        $tab['PartCible'] = number_format(($montantpartypeciblehmo / $montantaccorde) * 100, 2);
        $tab['PartOdd'] = number_format(($montantpartypeciblehmo / $montanttotalcible) * 100, 2);
        $tab['type'] = 'totalhmo';
        array_push($listes, $tab);

        $tab['ODD'] = "Total Dépenses contribuant aux ODD";
        $tab['Montant'] = $montantpartypeciblemo;
        $tab['PartCible'] = number_format(($montantpartypeciblemo / $montantaccorde) * 100, 2);
        $tab['PartOdd'] = number_format(($montantpartypeciblemo / $montanttotalcible) * 100, 2);
        $tab['type'] = 'tdcodd';
        array_push($listes, $tab);

        $tab['ODD'] = "BUDGET TOTAL";
        $tab['Montant'] = $montantaccorde;
        $tab['PartCible'] = "";
        $tab['PartOdd'] = "";
        $tab['type'] = 'budtot';
        array_push($listes, $tab);

        return new JsonResponse(array('statut' => 'succes', 'contenu' => $listes, 'dataCible' => $donutDataCible, 'dataOdd' => $donutDataOdd));

    }


    /**
     * @Route("/analyseBudgetaireOdd", name="ajax_analyse_budgetaire_odd")
     */
    public function analyseBudgetaireOddAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        // retrieve the file with the name given in the form.
        // do var_dump($request->files->all()); if you need to know if the file is being uploaded.

        $em = $this->getDoctrine()->getManager();

        $exercice = $request->get('exe');

        $ministere = $request->get('min');


//        dump($ministere);die();
        $montantaccorde = $em->getRepository('App:Execution')->sumActiviteExecutionByExerciceByMinistere($exercice, "BSV", $ministere)[0]['total'];

//        $montantaccorde = $em->getRepository('App:Execution')->sumActiviteExecutionByExerciceByMinistereByAnalyse($exercice,"BSV",$ministere)[0]['total'];

//        $montantexecute = $em->getRepository('AppBundle:Execution')->sumExecutionByMinistere($session->get('codeministere'),$session->get('exoid'))[0]['mont_exec'];

        $listesmontantparodd = $em->getRepository('App:Execution')->findMontantParOddByExerciceByMinistere($exercice, "BSV", $ministere);

        //HORS MISE EN OEUVRE

        $montantpartypeciblehmo = $em->getRepository('App:Execution')->findMontantParTypeCibleByExerciceByMinistere($exercice, "BSV", $ministere, "HMEO")[0]['montTypeCible'];

        //MISE EN OEUVRE
//        $montantpartypeciblemo = $em->getRepository('App:Execution')->findMontantParTypeCibleByExerciceByMinistere($exercice,"BSV",$ministere,"MEO")[0]['montTypeCible'];
        $montantpartypeciblemo = $em->getRepository('App:Execution')->sumMontantParOddByExerciceByMinistere($exercice, "BSV", $ministere)[0]['montOdd'];

//        dump($montantpartypeciblemo);die();
        $montanttotalcible = $montantpartypeciblemo;

        $listes = [];
        $donutDataOdd = [];
        $donutDataCible = [];

        foreach ($listesmontantparodd as $listemontantparodd) {

            $listesmontantparcibleodd = $em->getRepository('App:Execution')->findMontantParCibleOddByExerciceByMinistere($exercice, "BSV", $ministere, $listemontantparodd['oddCod']);

            foreach ($listesmontantparcibleodd as $listemontantparcibleodd) {

//                $listesmontantparcibleodd = $em->getRepository('AppBundle:Execution')->findMontantParCibleOddByExerciceByMinistere($session->get('exoid'),"BSV",$session->get('codeministere'),$listemontantparodd['id']);
                $tab['ODD'] = $listemontantparcibleodd['cibCod'];
                $tab['Montant'] = $listemontantparcibleodd['montCible'];
                $tab['PartCible'] = number_format(($listemontantparcibleodd['montCible'] / $montantaccorde) * 100, 2);
                $tab['PartOdd'] = number_format(($listemontantparcibleodd['montCible'] / $montanttotalcible) * 100, 2);
                $tab['type'] = 'cible';

                $tab1['label'] = $listemontantparcibleodd['cibCod'];
                $tab1['data'] = $listemontantparcibleodd['montCible'];
//                $tab1['color'] = '#0073b7';

//                dump($listesmontantparcibleodd);
                array_push($listes, $tab);
                array_push($donutDataCible, $tab1);
            }
            $tab['ODD'] = "Total " . $listemontantparodd['oddCod'];
            $tab['Montant'] = $listemontantparodd['montOdd'];
            $tab['PartCible'] = number_format(($listemontantparodd['montOdd'] / $montantaccorde) * 100, 2);
            $tab['PartOdd'] = number_format(($listemontantparodd['montOdd'] / $montanttotalcible) * 100, 2);
            $tab['type'] = 'odd';

            $tab1['label'] = $listemontantparodd['oddCod'];
            $tab1['data'] = $listemontantparodd['montOdd'];
//            $tab1['color'] = '#00c0ef';
            array_push($listes, $tab);

            array_push($donutDataOdd, $tab1);

        }
        $tab['ODD'] = "Total Hors Mise en Oeuvre";
        $tab['Montant'] = $montantpartypeciblehmo;
        $tab['PartCible'] = number_format(($montantpartypeciblehmo / $montantaccorde) * 100, 2);
        $tab['PartOdd'] = number_format(($montantpartypeciblehmo / $montanttotalcible) * 100, 2);
        $tab['type'] = 'totalhmo';
        array_push($listes, $tab);

        $tab['ODD'] = "Total Dépenses contribuant aux ODD";
        $tab['Montant'] = $montantpartypeciblemo;
        $tab['PartCible'] = number_format(($montantpartypeciblemo / $montantaccorde) * 100, 2);
        $tab['PartOdd'] = number_format(($montantpartypeciblemo / $montanttotalcible) * 100, 2);
        $tab['type'] = 'tdcodd';
        array_push($listes, $tab);

        $tab['ODD'] = "BUDGET TOTAL";
        $tab['Montant'] = $montantaccorde;
        $tab['PartCible'] = "";
        $tab['PartOdd'] = "";
        $tab['type'] = 'budtot';
        array_push($listes, $tab);

        $this->sessionVariable($exercice, $ministere, $listes, "", "");

        return new JsonResponse(array('statut' => 'succes', 'contenu' => $listes, 'dataCible' => $donutDataCible, 'dataOdd' => $donutDataOdd));

    }


    /**
     * @Route("/analyseBudgetaireOddAllMinistere", name="ajax_analyse_budgetaire_odd_all_ministere")
     */
    public function analyseBudgetaireOddAllMinistereAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        // retrieve the file with the name given in the form.
        // do var_dump($request->files->all()); if you need to know if the file is being uploaded.

        $em = $this->getDoctrine()->getManager();

        $exercice = $request->get('exe');

//        dump($ministere);die();
        $montantaccorde = $em->getRepository('App:Execution')->sumActiviteExecutionByExerciceAllMinistere($exercice, "BSV")[0]['total'];

//        $montantaccorde = $em->getRepository('App:Execution')->sumActiviteExecutionByExerciceByMinistereByAnalyse($exercice,"BSV",$ministere)[0]['total'];

//        $montantexecute = $em->getRepository('AppBundle:Execution')->sumExecutionByMinistere($session->get('codeministere'),$session->get('exoid'))[0]['mont_exec'];

        $listesmontantparodd = $em->getRepository('App:Execution')->findMontantParOddByExerciceAllMinistere($exercice, "BSV");

        //HORS MISE EN OEUVRE

        $montantpartypeciblehmo = $em->getRepository('App:Execution')->findMontantParTypeCibleByExerciceAllMinistere($exercice, "BSV", "HMEO")[0]['montTypeCible'];

        //MISE EN OEUVRE
//        $montantpartypeciblemo = $em->getRepository('App:Execution')->findMontantParTypeCibleByExerciceByMinistere($exercice,"BSV",$ministere,"MEO")[0]['montTypeCible'];
        $montantpartypeciblemo = $em->getRepository('App:Execution')->sumMontantParOddByExerciceAllMinstere($exercice, "BSV")[0]['montOdd'];

//        dump($montantpartypeciblemo);die();
        $montanttotalcible = $montantpartypeciblemo;

        $listes = [];
        $donutDataOdd = [];
        $donutDataCible = [];

        foreach ($listesmontantparodd as $listemontantparodd) {

            $listesmontantparcibleodd = $em->getRepository('App:Execution')->findMontantParCibleOddByExerciceAllMinistere($exercice, "BSV", $listemontantparodd['oddCod']);

            foreach ($listesmontantparcibleodd as $listemontantparcibleodd) {

//                $listesmontantparcibleodd = $em->getRepository('AppBundle:Execution')->findMontantParCibleOddByExerciceByMinistere($session->get('exoid'),"BSV",$session->get('codeministere'),$listemontantparodd['id']);
                $tab['ODD'] = $listemontantparcibleodd['cibCod'];
                $tab['Montant'] = $listemontantparcibleodd['montCible'];
                $tab['PartCible'] = number_format(($listemontantparcibleodd['montCible'] / $montantaccorde) * 100, 2);
                $tab['PartOdd'] = number_format(($listemontantparcibleodd['montCible'] / $montanttotalcible) * 100, 2);
                $tab['type'] = 'cible';

                $tab1['label'] = $listemontantparcibleodd['cibCod'];
                $tab1['data'] = $listemontantparcibleodd['montCible'];
//                $tab1['color'] = '#0073b7';

//                dump($listesmontantparcibleodd);
                array_push($listes, $tab);
                array_push($donutDataCible, $tab1);
            }
            $tab['ODD'] = "Total " . $listemontantparodd['oddCod'];
            $tab['Montant'] = $listemontantparodd['montOdd'];
            $tab['PartCible'] = number_format(($listemontantparodd['montOdd'] / $montantaccorde) * 100, 2);
            $tab['PartOdd'] = number_format(($listemontantparodd['montOdd'] / $montanttotalcible) * 100, 2);
            $tab['type'] = 'odd';

            $tab1['label'] = $listemontantparodd['oddCod'];
            $tab1['data'] = $listemontantparodd['montOdd'];
//            $tab1['color'] = '#00c0ef';
            array_push($listes, $tab);

            array_push($donutDataOdd, $tab1);

        }
        $tab['ODD'] = "Total Hors Mise en Oeuvre";
        $tab['Montant'] = $montantpartypeciblehmo;
        $tab['PartCible'] = number_format(($montantpartypeciblehmo / $montantaccorde) * 100, 2);
        $tab['PartOdd'] = number_format(($montantpartypeciblehmo / $montanttotalcible) * 100, 2);
        $tab['type'] = 'totalhmo';
        array_push($listes, $tab);

        $tab['ODD'] = "Total Dépenses contribuant aux ODD";
        $tab['Montant'] = $montantpartypeciblemo;
        $tab['PartCible'] = number_format(($montantpartypeciblemo / $montantaccorde) * 100, 2);
        $tab['PartOdd'] = number_format(($montantpartypeciblemo / $montanttotalcible) * 100, 2);
        $tab['type'] = 'tdcodd';
        array_push($listes, $tab);

        $tab['ODD'] = "BUDGET TOTAL";
        $tab['Montant'] = $montantaccorde;
        $tab['PartCible'] = "";
        $tab['PartOdd'] = "";
        $tab['type'] = 'budtot';
        array_push($listes, $tab);

        return new JsonResponse(array('statut' => 'succes', 'contenu' => $listes, 'dataCible' => $donutDataCible, 'dataOdd' => $donutDataOdd));

    }


    /**
     * @Route("/ajaxRepartitionBudgetOdd", name="ajax_repartition_budget_odd")
     */
    public function indexRepartitionBudgetOddMinistereAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

//        $session = new Session();

        $em = $this->getDoctrine()->getManager();

        $listesmontantparodd = $em->getRepository('App:Execution')->findMontantParOddByExercice($request->get('exe'), "BSV");

        $tabodd = [];
        $taboddmontant = [];
//        dump($listesmontantparodd);die();
        foreach ($listesmontantparodd as $listemontantparodd) {
            array_push($tabodd, $listemontantparodd['oddCod']);
            array_push($taboddmontant, $listemontantparodd['montOdd']);
        }


        return new JsonResponse(array('statut' => "true", 'tabodd' => $tabodd, 'taboddmontant' => $taboddmontant));

//        return $this->render('AjaxBundle:Default:index.html.twig');
    }


    /**
     * @Route("/ajaxDashBord", name="ajax_dash_bord")
     */
    public function indexAction(Request $request)
    {

//        dump($request);die();
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

//        $session = new Session();

        $em = $this->getDoctrine()->getManager();

        $listesmontantparodd = $em->getRepository('App:Execution')->findMontantParOddByExerciceByMinistere($request->get('exe'), "BSV", $request->get('min'));

        $tabodd = [];
        $taboddmontant = [];
//        dump($listesmontantparodd);die();
        foreach ($listesmontantparodd as $listemontantparodd) {
            array_push($tabodd, $listemontantparodd['oddCod']);
            array_push($taboddmontant, $listemontantparodd['montOdd']);
        }


        return new JsonResponse(array('statut' => "true", 'tabodd' => $tabodd, 'taboddmontant' => $taboddmontant));

//        return $this->render('AjaxBundle:Default:index.html.twig');
    }

    /**
     * @Route("/poweronExercice", name="ajax_on_exercice")
     */
    public function powerOnExerciceAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');


//        return $this->render('AjaxBundle:Default:index.html.twig');
    }


    /**
     * @Route("/enregistrerLigneExecution", name="ajax_enregistrer_ligne_execution")
     */
    public function enregistrerLigneExecution(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $session = new Session();

        $em = $this->getDoctrine()->getManager();

        $activAnalyse = $em->getRepository('App:Execution')->findActiviteExecutionByAnalyse($request->get('idanalyse'))[0];

        $montantExecute = $em->getRepository('App:Execution')->sumExecutionByBudget($request->get('idanalyse'))[0]['mont_exec'];

        $disponible = $activAnalyse['budMont'] - $montantExecute - $request->get('montant');

        if ($disponible < 0) {


            return new JsonResponse(array('statut' => "Disponible non suffisant", 'codeexec' => ""));

        } else {

            $execution = new Execution();

            $execution->setExecUserSai($session->get('nom') . ' ' . $session->get('prenom'));
            $execution->setExecExercice($em->getRepository('App:Exercice')->find($session->get('exercice')));
            $execution->setExecTrimestre($em->getRepository('App:Trimestre')->find($request->get('trimestre')));
            $execution->setExecAnalyse($em->getRepository('App:Analyse')->find($request->get('idanalyse')));
            $execution->setExecMont($request->get('montant'));
//dump($execution);die();
            $em->persist($execution);
            $em->flush();

            if ($execution->getId() == 0) {
                return new JsonResponse(array('statut' => "false", 'codeexec' => $execution->getId()));
            } else {
                return new JsonResponse(array('statut' => "true", 'codeexec' => $execution->getId()));
            }
        }


    }


    /**
     * @Route("/detailExecutionAnalyse", name="ajax_detail_execution_analyse")
     */
    public function detailExecutionAnalyseAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
//        $session = new Session();

        $em = $this->getDoctrine()->getManager();

        $listeExecutionAnalyse = $em->getRepository('App:Execution')->findExecutionByAnalyse($request->get('idanalyse'));

        $activAnalyse = $em->getRepository('App:Execution')->findActiviteExecutionByAnalyse($request->get('idanalyse'))[0];

        $montantExecute = $em->getRepository('App:Execution')->sumExecutionByBudget($request->get('idanalyse'))[0]['mont_exec'];

        $disponible = $activAnalyse['budMont'] - $montantExecute;
//        dump($listeExecutionAnalyse,$activAnalyse,$montantExecute);
//        die();
//        $etat = $em->getRepository('App:Budget')->valideActiviteBudgetByExercice($session->get('exoid'));


        return new JsonResponse(array('listeExecutionAnalyse' => $listeExecutionAnalyse,
            'activAnalyse' => $activAnalyse,
            'montantExecute' => $montantExecute,
            'disponible' => $disponible));
    }


    /**
     * @Route("/supprimerLigneExecution", name="ajax_suppression_ligne_execution")
     */
    public function supprimerLigneExecution(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $session = new Session();

        $em = $this->getDoctrine()->getManager();

        $execution = $em->getRepository('App:Execution')->find($request->get('idexect'));

//        dump($execution->getExecStatCod());die();
        if ($execution->getExecStatCod() == "EBSN") {
            $em->remove($execution);
            $em->flush();

            return new JsonResponse(array('statut' => true, 'montant' => $execution->getExecMont()));

        } else {


            return new JsonResponse(array('statut' => "Validé", 'montant' => $execution->getExecMont()));

        }


    }

    /**
     * @Route("/annuleeElaborationChargement", name="ajax_annule_charge")
     */
    public function annuleeElaborationChargementAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $session = new Session();

        $em = $this->getDoctrine()->getManager();

        $etat = $em->getRepository('App:Budget')->deleteActiviteBudgetByExercice($session->get('exercice'));

        return new JsonResponse(array('statut' => "Ok"));
    }


    /**
     * @Route("/valideElaborationChargement", name="ajax_valide_charge")
     */
    public function valideElaborationChargementAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $session = new Session();

        $em = $this->getDoctrine()->getManager();

        $etat = $em->getRepository('App:Budget')->valideActiviteBudgetByExercice($session->get('exercice'));

        return new JsonResponse(array('statut' => "Ok"));
    }


    /**
     * @Route("/chargeFichier", name="ajax_charge_fichier")
     */
    public function chargerFichierAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        // retrieve the file with the name given in the form.
        // do var_dump($request->files->all()); if you need to know if the file is being uploaded.
        $file = $request->files->get('file');

        $lignesFichier = [];
        $row = 1;

        if (($handle = fopen($file->getPathname(), "r")) !== FALSE) {
            $ligne = [];
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $laligne = [];
                if ($row != 1) {
                    for ($c = 0; $c < $num; $c++) {
                        $laligne = explode(";", $data[$c]);
//                        dump($laligne);die();
                        $ligne['code'] = $laligne[0];
                        $activite = $this->getDoctrine()->getRepository('App:Activite')->find($laligne[0]);
                        $ligne['libelle'] = $activite->getActivLib();
                        $ligne['montant'] = $laligne[1];
                    }
                    array_push($lignesFichier, $ligne);

                }
                $row++;

            }
            fclose($handle);
        }

        if ($row == 1) {
            return new JsonResponse(array('statut' => "Vérifier le fichier"));
        } else {
            return new JsonResponse(array('statut' => $lignesFichier));
        }

    }


    /**
     * @Route("/enregistrerFichierBudget", name="ajax_enregistrer_fichier_budget")
     */
    public function enregistrerFichierBudgetAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        // retrieve the file with the name given in the form.
        // do var_dump($request->files->all()); if you need to know if the file is being uploaded.
        $file = $request->files->get('file');

        $row = 1;

        $session = new Session();

//        dump($request->files->get('file'));die();
        if (($handle = fopen($file->getPathname(), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                if ($row != 1) {
                    for ($c = 0; $c < $num; $c++) {
                        $laligne = explode(";", $data[$c]);
                        $em = $this->getDoctrine()->getManager();
                        $budget = new Budget();
//                        dump($laligne[1],intval(str_replace(' ','',$laligne[1])));die();
                        $budget->setBudMont(intval(str_replace(' ', '', $laligne[1])));
                        $budget->setBudExercice($em->getRepository('App:Exercice')->find($session->get('exercice')));
                        $budget->setBudActivite($em->getRepository('App:Activite')->find($laligne[0]));
                        $budget->setBudUserSai($session->get('nom') . ' ' . $session->get('prenom'));

                        $em->persist($budget);
                        $em->flush();
//                        $ligne['code'] = $laligne[0];

//                        $ligne['montant'] = $laligne[1];
                    }

                }
                $row++;

            }
            fclose($handle);
        }

        if ($row == 1) {
            return new JsonResponse(array('statut' => "Vérifier le fichier"));
        } else {
            return new JsonResponse(array('statut' => "Enregistrer avec succès"));
        }

    }

    /**
     * @Route("/chargeFichierAnalyse", name="ajax_charge_fichier_analyse")
     */
    public function chargerFichierAnalyseAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        // retrieve the file with the name given in the form.
        // do var_dump($request->files->all()); if you need to know if the file is being uploaded.
        $file = $request->files->get('file');

        $lignesFichier = [];
        $row = 1;

        if (($handle = fopen($file->getPathname(), "r")) !== FALSE) {
            $ligne = [];
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $laligne = [];
                if ($row != 1) {
                    for ($c = 0; $c < $num; $c++) {
                        $laligne = explode(";", $data[$c]);
//                        dump($laligne);die();
                        $cible = $this->getDoctrine()->getRepository('App:Cible')->find(trim($laligne[0]));
                        $ligne['cible'] = $cible->getCibCod();
                        $activite = $this->getDoctrine()->getRepository('App:Activite')->find(trim($laligne[1]));
                        $ligne['activite'] = $activite->getActivLib();
                        $trimestre = $this->getDoctrine()->getRepository('App:Trimestre')->find(trim($laligne[2]));
                        $ligne['trimestre'] = $trimestre->getTrimLib();
                    }
                    array_push($lignesFichier, $ligne);

                }
                $row++;

            }
            fclose($handle);
        }

        if ($row == 1) {
            return new JsonResponse(array('statut' => "Vérifier le fichier"));
        } else {
            return new JsonResponse(array('statut' => $lignesFichier));
        }

    }


    /**
     * @Route("/enregistrerFichierAnalyse", name="ajax_enregistrer_fichier_analyse")
     */
    public function enregistrerFichierAnalyseAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        // retrieve the file with the name given in the form.
        // do var_dump($request->files->all()); if you need to know if the file is being uploaded.
        $file = $request->files->get('file');

        $row = 1;

        $session = new Session();

//        dump($request->files->get('file'));die();
        if (($handle = fopen($file->getPathname(), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                if ($row != 1) {
                    for ($c = 0; $c < $num; $c++) {
                        $laligne = explode(";", $data[$c]);
                        $em = $this->getDoctrine()->getManager();
                        $analyse = new Analyse();
                        $analyse->setAnalTrimestre($em->getRepository('App:Trimestre')->find($laligne[2]));
                        $analyse->setAnalExercice($em->getRepository('App:Exercice')->find($session->get('exercice')));
                        $analyse->setAnalActivite($em->getRepository('App:Activite')->find($laligne[1]));
                        $analyse->setAnalCible($em->getRepository('App:Cible')->find($laligne[0]));
                        $analyse->setAnalUserSai($session->get('nom') . ' ' . $session->get('prenom'));

                        $em->persist($analyse);
                        $em->flush();
                    }
                }
                $row++;
            }
            fclose($handle);
        }

        if ($row == 1) {
            return new JsonResponse(array('statut' => "Vérifier le fichier"));
        } else {
            return new JsonResponse(array('statut' => "Enregistrer avec succès"));
        }

    }


    /**
     * @Route("/annuleeAnalyseChargement", name="ajax_analyse_annule_charge")
     */
    public function annuleeAnalyseChargementAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $session = new Session();

        $em = $this->getDoctrine()->getManager();

        $etat = $em->getRepository('App:Analyse')->deleteActiviteAnalyseByExercice($session->get('exercice'));

        return new JsonResponse(array('statut' => "Ok"));
    }


}
