<?php

namespace App\Controller;

use App\Entity\Action;
use App\Form\ActionType;
use App\Repository\ActionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/action")
 */
class ActionController extends AbstractController
{
    public function serieAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $em = $this->getDoctrine()->getManager();

        $nbraction = count($em->getRepository('App:Action')->findAll()) + 1;

        $serie = "".$nbraction;
        while (strlen($serie) < 6) {
            $serie = "0".$serie;
        }

        $serie ="A".$serie;
        return $serie;
    }

    /**
     * @Route("/", name="action_index", methods={"GET"})
     */
    public function index(ActionRepository $actionRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('action/index.html.twig', [
            'actions' => $actionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="action_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $action = new Action();
        $form = $this->createForm(ActionType::class, $action);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();
            $entityManager = $this->getDoctrine()->getManager();
            $action->setActUserSai($session->get('nom'). ' ' .$session->get('prenom'));
            $action->setActCod($this->serieAction());
            $entityManager->persist($action);
            $entityManager->flush();

            if ($action->getActCod() == "") {
                $session->getFlashBag()->add('error', 'Erreur enregistrement !');
            } else {
                $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
            }
            return $this->redirectToRoute('action_index');
        }

        return $this->render('action/new.html.twig', [
            'action' => $action,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{actCod}", name="action_show", methods={"GET"})
     */
    public function show(Action $action): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('action/show.html.twig', [
            'action' => $action,
        ]);
    }

    /**
     * @Route("/{actCod}/edit", name="action_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Action $action): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(ActionType::class, $action);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session  = new Session();

            $action->setActUserModif($session->get('nom'). ' ' .$session->get('prenom'));
            $action->setActDateModif(new \DateTime());
            $this->getDoctrine()->getManager()->flush();
            if ($action->getActCod() == "") {
                $session->getFlashBag()->add('error', 'Erreur modification !');
            } else {
                $session->getFlashBag()->add('success', 'Modification effectuée avec succès !');
            }

            return $this->redirectToRoute('action_index');
        }

        return $this->render('action/edit.html.twig', [
            'action' => $action,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{actCod}", name="action_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Action $action): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($this->isCsrfTokenValid('delete'.$action->getActCod(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($action);
            $entityManager->flush();
        }

        return $this->redirectToRoute('action_index');
    }
}
