<?php

namespace App\Controller;

use App\Entity\Exercice;
use App\Entity\Ministere;
use App\Entity\Trimestre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * Statistique controller.
 *
 * @Route("statistique")
 */
class StatistiqueController extends Controller
{




    /**
     * @Route("/detailBudgetExecute", name="detail_budget_execute")
     */
    public function indexDetailExecuteAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $entityManager = $this->getDoctrine()->getManager();

//        $session = new Session();

        $exercices = $entityManager->getRepository(Exercice::class)->findAll();

        $exercices = array_reverse($exercices);

//        dump($exercices);die();
        // replace this example code with whatever you need
        return $this->render('statistiques/detail_budget_execute/index.html.twig',compact('exercices'));
    }


    /**
     * @Route("/detailBudgetPlanifie", name="detail_budget_planifie")
     */
    public function indexDetailBudgetPlanifieAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $entityManager = $this->getDoctrine()->getManager();

//        $session = new Session();

        $exercices = $entityManager->getRepository(Exercice::class)->findAll();

        $exercices = array_reverse($exercices);

//        dump($exercices);die();
        // replace this example code with whatever you need
        return $this->render('statistiques/detail_budget_planifie/index.html.twig',compact('exercices'));
    }


    /**
     * @Route("/situationExecutionOddCible", name="situation_execution_odd_cible")
     */
    public function indexSituationExecutionOddCibleAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $entityManager = $this->getDoctrine()->getManager();

//        $session = new Session();

        $exercices = $entityManager->getRepository(Exercice::class)->findAll();

        $exercices = array_reverse($exercices);
        $ministeres = $entityManager->getRepository(Ministere::class)->findAll();




//        dump($exercices);die();
        // replace this example code with whatever you need
        return $this->render('statistiques/ensemble_budgetaire_dun_ministere/index.html.twig',compact('exercices','ministeres'));
    }


    /**
     * @Route("/ensembleBudgetDunMinistere", name="ensemble_budget_dun_ministere")
     */
    public function indexEnsembleBudgetDuMinistereAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $entityManager = $this->getDoctrine()->getManager();

//        $session = new Session();

        $exercices = $entityManager->getRepository(Exercice::class)->findAll();

        $exercices = array_reverse($exercices);
        $ministeres = $entityManager->getRepository(Ministere::class)->findAll();




//        dump($exercices);die();
        // replace this example code with whatever you need
        return $this->render('statistiques/ensemble_budgetaire_dun_ministere/index.html.twig',compact('exercices','ministeres'));
    }

    /**
     * @Route("/ensembleBudgetMinistere", name="ensemble_budget_ministere")
     */
    public function indexEnsembleBudgetMinistereAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $entityManager = $this->getDoctrine()->getManager();

//        $session = new Session();

        $exercices = $entityManager->getRepository(Exercice::class)->findAll();

        $exercices = array_reverse($exercices);

//        dump($exercices);die();
        // replace this example code with whatever you need
        return $this->render('statistiques/ensemble_budgetaire/index.html.twig',compact('exercices'));
    }

    /**
     * @Route("/repartitionBudgetMinistere", name="repartition_budget_ministere")
     */
    public function indexRepartitionBudgetMinistereAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $entityManager = $this->getDoctrine()->getManager();

//        $session = new Session();

        $exercices = $entityManager->getRepository(Exercice::class)->findAll();

        $exercices = array_reverse($exercices);



//        dump($exercices);die();
        // replace this example code with whatever you need
        return $this->render('statistiques/repartition_budget_par_ministere/index.html.twig',compact('exercices'));
    }



    /**
     * @Route("/repartitionDetailleBudgetMinistere", name="repartition_detaille_budget_ministere")
     */
    public function indexRepartitionDetailleBudgetMinistereAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $entityManager = $this->getDoctrine()->getManager();

//        $session = new Session();

        $exercices = $entityManager->getRepository(Exercice::class)->findAll();
        $trimestres = $entityManager->getRepository(Trimestre::class)->findAll();

        $exercices = array_reverse($exercices);

//        dump($exercices);die();
        // replace this example code with whatever you need
        return $this->render('statistiques/repartition_detaille_budget_par_ministere/index.html.twig',compact('exercices','trimestres'));
    }

    /**
     * @Route("/analyseBudgetaireCible", name="analyse_budgetaire_cible")
     */
    public function indexAnalyseBudgetaireCibleAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $entityManager = $this->getDoctrine()->getManager();

//        $session = new Session();

        $exercices = $entityManager->getRepository(Exercice::class)->findAll();
        $trimestres = $entityManager->getRepository(Trimestre::class)->findAll();
        $ministeres = $entityManager->getRepository(Ministere::class)->findAll();

        $exercices = array_reverse($exercices);




//        dump($exercices);die();
        // replace this example code with whatever you need
        return $this->render('statistiques/analyse_budgetaire_cible/index.html.twig',compact('exercices','trimestres','ministeres'));
    }


    /**
     * @Route("/analyseBudgetaireOdd", name="analyse_budgetaire_odd")
     */
    public function indexAnalyseBudgetaireOddAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $entityManager = $this->getDoctrine()->getManager();

//        $session = new Session();

        $exercices = $entityManager->getRepository(Exercice::class)->findAll();
        $trimestres = $entityManager->getRepository(Trimestre::class)->findAll();
        $ministeres = $entityManager->getRepository(Ministere::class)->findAll();

        $exercices = array_reverse($exercices);




//        dump($exercices);die();
        // replace this example code with whatever you need
        return $this->render('statistiques/analyse_budgetaire_odd/index.html.twig',compact('exercices','trimestres','ministeres'));
    }



    /**
     * @Route("/analyseBudgetaireOddCible", name="analyse_budgetaire_odd_cible")
     */
    public function indexAnalyseBudgetaireOddCibleAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $entityManager = $this->getDoctrine()->getManager();

//        $session = new Session();

        $exercices = $entityManager->getRepository(Exercice::class)->findAll();
        $trimestres = $entityManager->getRepository(Trimestre::class)->findAll();
        $ministeres = $entityManager->getRepository(Ministere::class)->findAll();

        $exercices = array_reverse($exercices);




//        dump($exercices);die();
        // replace this example code with whatever you need
        return $this->render('statistiques/repartition_budgetaire_odd_cible/index.html.twig',compact('exercices','trimestres','ministeres'));
    }


    /**
     * @Route("/analyseBudgetaireMinistere", name="analyse_budgetaire_ministere")
     */
    public function indexAnalyseBudgetaireMinistereAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $entityManager = $this->getDoctrine()->getManager();

//        $session = new Session();

        $exercices = $entityManager->getRepository(Exercice::class)->findAll();
        $ministeres = $entityManager->getRepository(Ministere::class)->findAll();
        $trimestres = $entityManager->getRepository(Trimestre::class)->findAll();

        $exercices = array_reverse($exercices);




//        dump($exercices);die();
        // replace this example code with whatever you need
        return $this->render('statistiques/analyse_budgetaire_dun_ministere/index.html.twig',compact('exercices','trimestres','ministeres'));
    }

}
