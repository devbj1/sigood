<?php

namespace App\Controller;

use App\Entity\Odd;
use App\Form\OddType;
use App\Repository\OddRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/odd")
 */
class OddController extends AbstractController
{

    public function serieAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $em = $this->getDoctrine()->getManager();

        $nbrodd = count($em->getRepository('AppBundle:Odd')->findAll()) + 1;

        $serie = "".$nbrodd;
        while (strlen($serie) < 6) {
            $serie = "0".$serie;
        }

        $serie ="ODD".$serie;
        return $serie;
    }

    /**
     * @Route("/", name="odd_index", methods={"GET"})
     */
    public function index(OddRepository $oddRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('odd/index.html.twig', [
            'odds' => $oddRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="odd_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $odd = new Odd();
        $form = $this->createForm(OddType::class, $odd);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();
            $entityManager = $this->getDoctrine()->getManager();
            $odd->setOddUserSai($session->get('nom'). ' ' .$session->get('prenom'));
            $odd->setOddCod($this->serieAction());
            $entityManager->persist($odd);
            $entityManager->flush();

            $session = new Session();
            if ($odd->getOddCod() == "") {
                $session->getFlashBag()->add('error', 'Erreur enregistrement !');
            } else {
                $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
            }



            return $this->redirectToRoute('odd_index');
        }

        return $this->render('odd/new.html.twig', [
            'odd' => $odd,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{oddCod}", name="odd_show", methods={"GET"})
     */
    public function show(Odd $odd): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('odd/show.html.twig', [
            'odd' => $odd,
        ]);
    }

    /**
     * @Route("/{oddCod}/edit", name="odd_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Odd $odd): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(OddType::class, $odd);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session  = new Session();

            $odd->setOddUserModif($session->get('nom'). ' ' .$session->get('prenom'));
            $odd->setOddDatModif(new \DateTime());
            $this->getDoctrine()->getManager()->flush();
            if ($odd->getOddCod() == "") {
                $session->getFlashBag()->add('error', 'Erreur modification !');
            } else {
                $session->getFlashBag()->add('success', 'Modification effectuée avec succès !');
            }
            return $this->redirectToRoute('odd_index');
        }

        return $this->render('odd/edit.html.twig', [
            'odd' => $odd,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{oddCod}", name="odd_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Odd $odd): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($this->isCsrfTokenValid('delete'.$odd->getOddCod(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($odd);
            $entityManager->flush();
        }

        return $this->redirectToRoute('odd_index');
    }
}
