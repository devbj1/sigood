
    function detailglobal() {

        $.ajax({
                url: detailRepartitionGlobalBudgetUrl,
                type: "POST",
                data: {
                    'exe': $("#comboexercice").val(),
                },
                success: function (data) {

                    // var donutDataOdd = data['dataOdd'];

                    var donutDataRepartition = data['dataRep'];

                    // console.log(donutDataRepartition)
                    $.plot('#donut-chart-repartition-global', donutDataRepartition, {
                        series: {
                            pie: {
                                show: true,
                                radius: 1,
                                innerRadius: 0.5,
                                label: {
                                    show: true,
                                    radius: 2 / 3,
                                    formatter: labelFormatter,
                                    threshold: 0.1
                                }

                            }
                        },
                        legend: {
                            show: true
                        }
                    });

                    /*
                     * Custom Label formatter
                     * ----------------------
                     */
                    function labelFormatter(label, series) {
                        return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
                            + label
                            + '<br>'
                            + Math.round(series.percent) + '%</div>'
                    }


                }

                // tableadddonnevalide.clear();
                // boutonselectionner.button('reset');
            }
        );
    }

    $("#comboexercice").change(function (e) {
        e.preventDefault();

        cleanchar();

        detailglobal();

    });



    function cleanchar() {

        // $('#donut-chart-odd').remove(); // this is my <canvas> element
        $('#donut-chart-repartition-global').remove(); // this is my <canvas> element

        // $('#chart-odd').append('<div id="donut-chart-odd" style="height: 300px;"></div>');
        $('#chart-repartition-global').append('<div id="donut-chart-repartition-global" style="height: 300px;"></div>');

    }

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split(',');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
