function detailglobal() {
    $('#lignedetailbudgetexecute').html("<tr>\n" +
        "                            <th class=\"col-md-2\">Code</th>\n" +
        "                            <th class=\"col-md-6\">Libellé</th>\n" +
        "                            <th class=\"col-md-1\">Montant Planifié</th>\n" +
        "                            <th class=\"col-md-1\">Montant Executé</th>\n" +
        "                            <th class=\"col-md-1\">Montant Disponible</th>\n" +
        "                            <th class=\"col-md-1\">Taux (%)</th>\n" +
        "                        </tr>");
    $.ajax({
            url: detailBudgetPlanifieExecutionUrl,
            type: "POST",
            data: {
                'exe': $("#comboexercice").val(),
                'min': $("#comboministere").val(),
            },
            success: function (data) {

                // var donutDataOdd = data['dataOdd'];

                // var donutDataCible = data['dataRep'];

                data['dataDetBud'].forEach(function (g) {

                    var montantexecute = g['montantexecute'];
                    // var montantdisponible = g['montantdisponible'];
                    // var pourcentage = g['montantplanifie']/g['montantexecute'];

                    if (montantexecute==null){
                        montantexecute = 0
                    }

                    if (g['type'] == "min"){

                        $('#lignedetailbudgetexecute').append("<tr>\n" +
                            "                            <td>" + g['code'] + "</td>\n" +
                            "                            <td>" + g['libelle'] + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['montantplanifie']) + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(montantexecute) + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['montantdisponible']) + "</td>\n" +
                            "                            <td style=\"text-align: center\">" + g['pourcentage'] + "%</td>\n" +
                            "                        </tr>");
                    }

                    if (g['type'] == "prog"){

                        $('#lignedetailbudgetexecute').append("<tr>\n" +
                            "                            <td>&nbsp;&nbsp;&nbsp;" + g['code'] + "</td>\n" +
                            "                            <td>" + g['libelle'] + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['montantplanifie']) + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(montantexecute) + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['montantdisponible']) + "</td>\n" +
                            "                            <td style=\"text-align: center\">" + g['pourcentage'] + "%</td>\n" +
                            "                        </tr>");
                    }

                    if (g['type'] == "act"){

                        $('#lignedetailbudgetexecute').append("<tr>\n" +
                            "                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + g['code'] + "</td>\n" +
                            "                            <td>" + g['libelle'] + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['montantplanifie']) + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(montantexecute) + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['montantdisponible']) + "</td>\n" +
                            "                            <td style=\"text-align: center\">" + g['pourcentage'] + "%</td>\n" +
                            "                        </tr>");
                    }


                    if (g['type'] == "activ"){

                        $('#lignedetailbudgetexecute').append("<tr>\n" +
                            "                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + g['code'] + "</td>\n" +
                            "                            <td>" + g['libelle'] + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['montantplanifie']) + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(montantexecute) + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['montantdisponible']) + "</td>\n" +
                            "                            <td style=\"text-align: center\">" + g['pourcentage'] + "%</td>\n" +
                            "                        </tr>");
                    }

                    // console.log(g.code);
                });




            }

            // tableadddonnevalide.clear();
            // boutonselectionner.button('reset');
        }
    );
}



$("#comboexercice").change(function (e) {
    e.preventDefault();


    cleanchar();

    detailglobal();


});


function cleanchar() {

    // $('#donut-chart-odd').remove(); // this is my <canvas> element
    $('#donut-chart-cible').remove(); // this is my <canvas> element

    // $('#chart-odd').append('<div id="donut-chart-odd" style="height: 300px;"></div>');
    $('#chart-cible').append('<div id="donut-chart-cible" style="height: 300px;"></div>');

}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split(',');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}