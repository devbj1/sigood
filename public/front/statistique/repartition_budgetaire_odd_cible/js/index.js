function detailglobal() {
    $('#lignedetailanalyseodd').html("<tr>\n" +
        "                            <th>ODD</th>\n" +
        "                            <th>Montant (Millions)</th>\n" +
        "                            <th>Part dans le Budget Odd (%)</th>\n" +
        "                        </tr>");

    $('#lignedetailanalysecible').html("<tr>\n" +
        "                            <th>Cible</th>\n" +
        "                            <th>Montant (Millions)</th>\n" +
        "                            <th>Part dans le Budget Odd (%)</th>\n" +
        "                        </tr>");

    $.ajax({
            url: detailAnalyseBudgetUrl,
            type: "POST",
            data: {
                'exe': $("#comboexercice").val(),
                'min': $("#comboministere").val(),
            },
            success: function (data) {

                var donutDataOdd = data['dataOdd'];

                var donutDataCible = data['dataCible'];

                data['contenu'].forEach(function (g) {

                    console.log("ok")
                    if (g['type'] == "odd" || g['type'] == "budtot") {
                        console.log("ok1")

                        $('#lignedetailanalyseodd').append("<tr>\n" +
                            "                            <td>" + g['ODD'] + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['Montant']) + "</td>\n" +
                            "                            <td style=\"text-align: center\"><span class=\"badge bg-blue\">" + g['PartOdd'] + "</span></td>\n" +
                            "                        </tr>");
                    }

                    if (g['type'] == "cible" || g['type'] == "budtot") {
                        console.log("ok2")

                        $('#lignedetailanalysecible').append("<tr>\n" +
                            "                            <td>" + g['ODD'] + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['Montant']) + "</td>\n" +
                            "                            <td style=\"text-align: center\"><span class=\"badge bg-blue\">" + g['PartOdd'] + "</span></td>\n" +
                            "                        </tr>");
                    }
                    // console.log(g.code);
                });


                $.plot('#donut-chart-odd', donutDataOdd, {
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            innerRadius: 0.5,
                            label: {
                                show: true,
                                radius: 2 / 3,
                                formatter: labelFormatter,
                                threshold: 0.1
                            }

                        }
                    },
                    legend: {
                        show: true
                    }
                });


                $.plot('#donut-chart-cible', donutDataCible, {
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            innerRadius: 0.5,
                            label: {
                                show: true,
                                radius: 2 / 3,
                                formatter: labelFormatter,
                                threshold: 0.1
                            }

                        }
                    },
                    legend: {
                        show: true
                    }
                });


                // $.plot('#donut-chart-cible', donutDataCible, {
                //     series: {
                //         pie: {
                //             show: true,
                //             radius: 1,
                //             innerRadius: 0.5,
                //             label: {
                //                 show: true,
                //                 radius: 2 / 3,
                //                 formatter: labelFormatter,
                //                 threshold: 0.1
                //             }
                //
                //         }
                //     },
                //     legend: {
                //         show: true
                //     }
                // });

                /*
                 * Custom Label formatter
                 * ----------------------
                 */
                function labelFormatter(label, series) {
                    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
                        + label
                        + '<br>'
                        + Math.round(series.percent) + '%</div>'
                }


            }

            // tableadddonnevalide.clear();
            // boutonselectionner.button('reset');
        }
    );


}


function detailglobaltrimestrielle() {
    $('#lignedetailanalyseodd').html("<tr>\n" +
        "                            <th>ODD</th>\n" +
        "                            <th>Montant (Millions)</th>\n" +
        "                            <th>Part dans le Budget Odd (%)</th>\n" +
        "                        </tr>");
    $.ajax({
            url: detailAnalyseBudgetPeriodeUrl,
            type: "POST",
            data: {
                'exe': $("#comboexercice").val(),
                'trimestre': $("#comboperiode").val(),
                'min': $("#comboministere").val(),
            },
            success: function (data) {

                var donutDataOdd = data['dataOdd'];

                // var donutDataCible = data['dataCible'];

                data['contenu'].forEach(function (g) {
                    if (g['type'] == "odd" || g['type'] == "budtot") {
                        $('#lignedetailanalyseodd').append("<tr>\n" +
                            "                            <td>" + g['ODD'] + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['Montant']) + "</td>\n" +
                            "                            <td style=\"text-align: center\"><span class=\"badge bg-blue\">" + g['PartOdd'] + "</span></td>\n" +
                            "                        </tr>");
                    }

                    // console.log(g.code);
                });


                $.plot('#donut-chart-odd', donutDataOdd, {
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            innerRadius: 0.5,
                            label: {
                                show: true,
                                radius: 2 / 3,
                                formatter: labelFormatter,
                                threshold: 0.1
                            }

                        }
                    },
                    legend: {
                        show: true
                    }
                });


                var donutDataCible = data['dataCible'];

                data['contenu'].forEach(function (g) {
                    if (g['type'] == "cible" || g['type'] == "budtot") {
                        $('#lignedetailanalysecible').append("<tr>\n" +
                            "                            <td>" + g['ODD'] + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['Montant']) + "</td>\n" +
                            "                            <td style=\"text-align: center\"><span class=\"badge bg-blue\">" + g['PartOdd'] + "</span></td>\n" +
                            "                        </tr>");
                    }

                    // console.log(g.code);
                });


                // $.plot('#donut-chart-odd', donutDataOdd, {
                //     series: {
                //         pie: {
                //             show: true,
                //             radius: 1,
                //             innerRadius: 0.5,
                //             label: {
                //                 show: true,
                //                 radius: 2 / 3,
                //                 formatter: labelFormatter,
                //                 threshold: 0.1
                //             }
                //
                //         }
                //     },
                //     legend: {
                //         show: true
                //     }
                // });


                $.plot('#donut-chart-cible', donutDataCible, {
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            innerRadius: 0.5,
                            label: {
                                show: true,
                                radius: 2 / 3,
                                formatter: labelFormatter,
                                threshold: 0.1
                            }

                        }
                    },
                    legend: {
                        show: true
                    }
                });


                /*
                 * Custom Label formatter
                 * ----------------------
                 */
                function labelFormatter(label, series) {
                    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
                        + label
                        + '<br>'
                        + Math.round(series.percent) + '%</div>'
                }


                // tableadddonnevalide.clear();
                // boutonselectionner.button('reset');
            }
        }
    );


}


$("#comboexercice").change(function (e) {
    e.preventDefault();


    cleanchar();

    // if ($("#comboexercice").val() == 0) {
    //
    //
    // } else {

    if ($("#comboperiode").val() == 0) {
        detailglobal();

    } else {
        detailglobaltrimestrielle();

    }

    // }


});


$("#comboministere").change(function (e) {
    e.preventDefault();


    cleanchar();

    // if ($("#comboexercice").val() == 0) {
    //
    //
    // } else {

    if ($("#comboperiode").val() == 0) {
        detailglobal();

    } else {
        detailglobaltrimestrielle();

    }

    // }


});

$("#comboperiode").change(function (e) {
    e.preventDefault();

    cleanchar();


    if ($("#comboexercice").val() == 0) {


    } else {

        if ($("#comboperiode").val() == 0) {
            detailglobal();

        } else {
            detailglobaltrimestrielle();

        }

    }


});


function cleanchar() {

    $('#donut-chart-odd').remove(); // this is my <canvas> element
    // $('#donut-chart-cible').remove(); // this is my <canvas> element

    $('#chart-odd').append('<div id="donut-chart-odd" style="height: 300px;"></div>');
    // $('#chart-cible').append('<div id="donut-chart-cible" style="height: 300px;"></div>');

}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split(',');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}