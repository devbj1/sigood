


    function detailglobal() {

        var ctx = document.getElementById('chart-repartition');

        $.ajax({
                url: detailRepatitionBudgetUrl,
                type: "POST",
                data: {
                    'exe': $("#comboexercice").val(),
                },
                success: function (data) {

                    // console.log(data);
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: data['tabodd'],
                            datasets: [{
                                label: 'Répartition par Odd',
                                data: data['taboddmontant'],
                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(54, 162, 235, 0.2)',
                                    'rgba(255, 206, 86, 0.2)',
                                    'rgba(75, 192, 192, 0.2)',
                                    'rgba(153, 102, 255, 0.2)',
                                    'rgba(255, 159, 120, 0.2)',
                                    'rgba(255, 159, 64, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255, 99, 132, 1)',
                                    'rgba(54, 162, 235, 1)',
                                    'rgba(255, 206, 86, 1)',
                                    'rgba(75, 192, 192, 1)',
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(255, 159, 120, 1)',
                                    'rgba(255, 159, 64, 1)'
                                ],
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        callback: function(value, index, values) {
                                            if(parseInt(value) >= 1000){
                                                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") +' F CFA';
                                            } else {
                                                return value +' F CFA';
                                            }
                                        }
                                    }
                                }]
                            }
                        }
                    });

                }

                // tableadddonnevalide.clear();
                // boutonselectionner.button('reset');
            }
        );
    }


    $("#comboexercice").change(function (e) {
        e.preventDefault();


        cleanchar();

        detailglobal();


    });


    function cleanchar() {

        // $('#donut-chart-odd').remove(); // this is my <canvas> element
        $('#chart-repartition').remove(); // this is my <canvas> element

        // $('#chart-odd').append('<div id="donut-chart-odd" style="height: 300px;"></div>');
        $('#graphe-repartition').append('<canvas id="chart-repartition" width="400" height="150"></canvas>');

    }

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split(',');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
