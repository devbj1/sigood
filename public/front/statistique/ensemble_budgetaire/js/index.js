function detailglobal() {

    var ctx = document.getElementById('chart-repartition');

    var exercice = $("#comboexercice").val();

    var ministere = $("#comboministere").val();

    $.ajax({
            url: detailEnsembleBudgetaireUrl,
            type: "POST",
            data: {
                'exe': exercice,
            },
            success: function (data) {

                // console.log(data);
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: data['tabodd'],
                        datasets: [{
                            label: 'Répartition par Odd',
                            data: data['taboddmontant'],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 120, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 120, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value, index, values) {
                                        if (parseInt(value) >= 1000) {
                                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' F CFA';
                                        } else {
                                            return value + ' F CFA';
                                        }
                                    }
                                }
                            }]
                        }
                    }
                });

            }

            // tableadddonnevalide.clear();
            // boutonselectionner.button('reset');
        }
    );

//Remplis tableau Odd Cible
    $('#lignedetailanalysecible').html("<tr>\n" +
        "                            <th>Cible</th>\n" +
        "                            <th>Montant (Millions)</th>\n" +
        "                            <th>Part dans le Budget Odd (%)</th>\n" +
        "                        </tr>");

    $('#lignedetailanalyseodd').html("<tr>\n" +
        "                            <th>ODD</th>\n" +
        "                            <th>Montant (Millions)</th>\n" +
        "                            <th>Part dans le Budget Odd (%)</th>\n" +
        "                        </tr>");

    $.ajax({
            url: detailAnalyseBudgetUrl,
            type: "POST",
            data: {
                'exe': exercice,
            },
            success: function (data) {

                // var donutDataOdd = data['dataOdd'];

                // var donutDataCible = data['dataCible'];

                data['contenu'].forEach(function (g) {
                    if (g['type'] == "cible" || g['type'] == "budtot"){

                        $('#lignedetailanalysecible').append("<tr>\n" +
                            "                            <td>" + g['ODD'] + "</td>\n" +
                            "                            <td>" + addCommas(g['Montant']) + "</td>\n" +
                            "                            <td><span class=\"badge bg-aqua\">" + g['PartOdd'] + "</span></td>\n" +
                            "                        </tr>");
                    }

                    if (g['type'] == "odd" || g['type'] == "budtot"){

                        $('#lignedetailanalyseodd').append("<tr>\n" +
                            "                            <td>" + g['ODD'] + "</td>\n" +
                            "                            <td>" + addCommas(g['Montant']) + "</td>\n" +
                            "                            <td><span class=\"badge bg-yellow-active\">" + g['PartOdd'] + "</span></td>\n" +
                            "                        </tr>");
                    }

                    // console.log(g.code);
                });

                // $.plot('#donut-chart-odd', donutDataOdd, {
                //     series: {
                //         pie: {
                //             show: true,
                //             radius: 1,
                //             innerRadius: 0.5,
                //             label: {
                //                 show: true,
                //                 radius: 2 / 3,
                //                 formatter: labelFormatter,
                //                 threshold: 0.1
                //             }
                //
                //         }
                //     },
                //     legend: {
                //         show: true
                //     }
                // });

                //
                // $.plot('#donut-chart-cible', donutDataCible, {
                //     series: {
                //         pie: {
                //             show: true,
                //             radius: 1,
                //             innerRadius: 0.5,
                //             label: {
                //                 show: true,
                //                 radius: 2 / 3,
                //                 formatter: labelFormatter,
                //                 threshold: 0.1
                //             }
                //
                //         }
                //     },
                //     legend: {
                //         show: true
                //     }
                // });

                /*
                 * Custom Label formatter
                 * ----------------------
                 */
                // function labelFormatter(label, series) {
                //     return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
                //         + label
                //         + '<br>'
                //         + Math.round(series.percent) + '%</div>'
                // }


            }

            // tableadddonnevalide.clear();
            // boutonselectionner.button('reset');
        }
    );

}


$("#comboexercice").change(function (e) {
    e.preventDefault();


    cleanchar();

    detailglobal();


});


$("#comboministere").change(function (e) {
    e.preventDefault();


    cleanchar();

    detailglobal();


});

function cleanchar() {

    // $('#donut-chart-odd').remove(); // this is my <canvas> element
    $('#chart-repartition').remove(); // this is my <canvas> element

    // $('#chart-odd').append('<div id="donut-chart-odd" style="height: 300px;"></div>');
    $('#graphe-repartition').append('<canvas id="chart-repartition" width="400" height="150"></canvas>');

}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split(',');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
