
    function detailglobal() {
        $('#lignedetailrepartitionbudgetministere').html("                        <tr>\n" +
            "                            <th class=\"col-md-6\">Ministere</th>\n" +
            "                            <th class=\"col-md-2\">Montant (Millions)</th>\n" +
            "                            <th class=\"col-md-2\">Part total des dépenses ODD (%)</th>\n" +
            "                            <th class=\"col-md-2\">Part des dépenses ODD dans le budget total(%)</th>\n" +
            "                        </tr>\n");
        $.ajax({
                url: detailRepartitionBudgetUrl,
                type: "POST",
                data: {
                    'exe': $("#comboexercice").val(),
                },
                success: function (data) {

                    // var donutDataOdd = data['dataOdd'];

                    var donutDataRepartition = data['dataCible'];

                    // console.log(donutDataRepartition)
                    data['contenu'].forEach(function (g) {
                        // if (g['type'] == "cible" || g['type'] == "budtot"){

                        $('#lignedetailrepartitionbudgetministere').append("<tr>\n" +
                            "                            <td>" + g['Ministere'] + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['BudMin']) + "</td>\n" +
                            "                            <td><span class=\"badge bg-blue\">" + g['PartOddDepense'] + "</span></td>\n" +
                            "                            <td><span class=\"badge bg-blue\">" + g['PartOddBudget'] + "</span></td>\n" +
                            "                        </tr>");
                        // }

                        // console.log(g.code);
                    });

                    // $.plot('#donut-chart-odd', donutDataOdd, {
                    //     series: {
                    //         pie: {
                    //             show: true,
                    //             radius: 1,
                    //             innerRadius: 0.5,
                    //             label: {
                    //                 show: true,
                    //                 radius: 2 / 3,
                    //                 formatter: labelFormatter,
                    //                 threshold: 0.1
                    //             }
                    //
                    //         }
                    //     },
                    //     legend: {
                    //         show: true
                    //     }
                    // });

                    //
                    $.plot('#donut-chart-repartition', donutDataRepartition, {
                        series: {
                            pie: {
                                show: true,
                                radius: 1,
                                innerRadius: 0.5,
                                label: {
                                    show: true,
                                    radius: 2 / 3,
                                    formatter: labelFormatter,
                                    threshold: 0.1
                                }

                            }
                        },
                        legend: {
                            show: true
                        }
                    });

                    /*
                     * Custom Label formatter
                     * ----------------------
                     */
                    function labelFormatter(label, series) {
                        return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
                            + label
                            + '<br>'
                            + Math.round(series.percent) + '%</div>'
                    }


                }

                // tableadddonnevalide.clear();
                // boutonselectionner.button('reset');
            }
        );
    }


    function detailglobaltrimestrielle() {
        $('#lignedetailrepartitionbudgetministere').html("                        <tr>\n" +
            "                            <th class=\"col-md-4\">Ministere</th>\n" +
            "                            <th class=\"col-md-4\">Montant (Millions)</th>\n" +
            "                            <th class=\"col-md-2\">Part total des dépenses ODD (%)</th>\n" +
            "                            <th class=\"col-md-2\">Part des dépenses ODD dans le budget total(%)</th>\n" +
            "                        </tr>\n");
        $.ajax({
                url: detailRepartitionBudgetPeriodeUrl,
                type: "POST",
                data: {
                    'exe': $("#comboexercice").val(),
                    'trimestre': $("#comboperiode").val(),
                },
                success: function (data) {

                    // var donutDataOdd = data['dataOdd'];

                    var donutDataRepartition = data['dataRepartition'];

                    data['contenu'].forEach(function (g) {
                        if (g['type'] == "cible" || g['type'] == "budtot") {
                            $('#lignedetailanalysecible').append("<tr>\n" +
                                "                            <td>" + g['ODD'] + "</td>\n" +
                                "                            <td style=\"text-align: right\">" + addCommas(g['Montant']) + "</td>\n" +
                                "                            <td><span class=\"badge bg-blue\">" + g['PartCible'] + "</span></td>\n" +
                                "                        </tr>");
                        }

                        // console.log(g.code);
                    });


                    // $.plot('#donut-chart-odd', donutDataOdd, {
                    //     series: {
                    //         pie: {
                    //             show: true,
                    //             radius: 1,
                    //             innerRadius: 0.5,
                    //             label: {
                    //                 show: true,
                    //                 radius: 2 / 3,
                    //                 formatter: labelFormatter,
                    //                 threshold: 0.1
                    //             }
                    //
                    //         }
                    //     },
                    //     legend: {
                    //         show: true
                    //     }
                    // });


                    $.plot('#donut-chart-repartition', donutDataRepartition, {
                        series: {
                            pie: {
                                show: true,
                                radius: 1,
                                innerRadius: 0.5,
                                label: {
                                    show: true,
                                    radius: 2 / 3,
                                    formatter: labelFormatter,
                                    threshold: 0.1
                                }

                            }
                        },
                        legend: {
                            show: true
                        }
                    });

                    /*
                     * Custom Label formatter
                     * ----------------------
                     */
                    function labelFormatter(label, series) {
                        return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
                            + label
                            + '<br>'
                            + Math.round(series.percent) + '%</div>'
                    }


                    // tableadddonnevalide.clear();
                    // boutonselectionner.button('reset');
                }
            }
        );
    }


    $("#comboexercice").change(function (e) {
        e.preventDefault();


        cleanchar();

        // if ($("#comboexercice").val() == 0) {
        //
        //
        // } else {

        if ($("#comboperiode").val() == 0) {
            detailglobal();

        } else {
            detailglobaltrimestrielle();

        }

        // }


    });


    $("#comboperiode").change(function (e) {
        e.preventDefault();

        cleanchar();


        if ($("#comboexercice").val() == 0) {


        } else {

            if ($("#comboperiode").val() == 0) {
                detailglobal();

            } else {
                detailglobaltrimestrielle();

            }

        }


    });


    function cleanchar() {

        // $('#donut-chart-odd').remove(); // this is my <canvas> element
        $('#donut-chart-repartition').remove(); // this is my <canvas> element

        // $('#chart-odd').append('<div id="donut-chart-odd" style="height: 300px;"></div>');
        $('#chart-repartition').append('<div id="donut-chart-repartition" style="height: 300px;"></div>');

    }

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split(',');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
