function detailglobal() {
    $('#lignedetailbudgetplanifie').html("<tr>\n" +
        "                            <th class=\"col-md-2\">Code</th>\n" +
        "                            <th class=\"col-md-9\">Libellé</th>\n" +
        "                            <th class=\"col-md-1\">Montant</th>\n" +
        "                        </tr>");
    $.ajax({
            url: detailBudgetPlanifieUrl,
            type: "POST",
            data: {
                'exe': $("#comboexercice").val(),
                'min': $("#comboministere").val(),
            },
            success: function (data) {

                // var donutDataOdd = data['dataOdd'];

                // var donutDataCible = data['dataRep'];

                data['dataDetBud'].forEach(function (g) {
                    if (g['type'] == "min"){

                        $('#lignedetailbudgetplanifie').append("<tr>\n" +
                            "                            <td>" + g['code'] + "</td>\n" +
                            "                            <td>" + g['libelle'] + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['montant']) + "</td>\n" +
                            "                        </tr>");
                    }

                    if (g['type'] == "prog"){

                        $('#lignedetailbudgetplanifie').append("<tr>\n" +
                            "                            <td>&nbsp;&nbsp;&nbsp;" + g['code'] + "</td>\n" +
                            "                            <td>" + g['libelle'] + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['montant']) + "</td>\n" +
                            "                        </tr>");
                    }

                    if (g['type'] == "act"){

                        $('#lignedetailbudgetplanifie').append("<tr>\n" +
                            "                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + g['code'] + "</td>\n" +
                            "                            <td>" + g['libelle'] + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['montant']) + "</td>\n" +
                            "                        </tr>");
                    }


                    if (g['type'] == "activ"){

                        $('#lignedetailbudgetplanifie').append("<tr>\n" +
                            "                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + g['code'] + "</td>\n" +
                            "                            <td>" + g['libelle'] + "</td>\n" +
                            "                            <td style=\"text-align: right\">" + addCommas(g['montant']) + "</td>\n" +
                            "                        </tr>");
                    }

                    // console.log(g.code);
                });




            }

            // tableadddonnevalide.clear();
            // boutonselectionner.button('reset');
        }
    );
}



$("#comboexercice").change(function (e) {
    e.preventDefault();


    cleanchar();

    detailglobal();


});


function cleanchar() {

    // $('#donut-chart-odd').remove(); // this is my <canvas> element
    $('#donut-chart-cible').remove(); // this is my <canvas> element

    // $('#chart-odd').append('<div id="donut-chart-odd" style="height: 300px;"></div>');
    $('#chart-cible').append('<div id="donut-chart-cible" style="height: 300px;"></div>');

}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split(',');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}