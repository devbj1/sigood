var ModalDetailAnalyse= $('#modal-detail-analyse');

var tableaddexecution = $("#tablelisteexecution").DataTable({
    'order': [[0, 'desc']],
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun résultat trouvé",
        "sEmptyTable": "Aucune donnée disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucune ligne affichée",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant", "sSortDescending": ": Trier par ordre décroissant"
        }
    }

});

function addCommas(nStr)
{
    nStr += '';
    x = nStr.split(',');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
// var form = document.getElementById("myform");
// or with jQuery
//var form = $("#myform")[0];

// $('#fichier').on('click', function (e) {
//     e.preventDefault();
function videmodaldetailanalyse() {
    ModalDetailAnalyse.find('#montantoctroye').val('');
    ModalDetailAnalyse.find('#montantexecute').val('');
    ModalDetailAnalyse.find('#disponible').val('');
    ModalDetailAnalyse.find('#liste-depense').val('');
}



$("#ajoutmontant").on('click', function (e) {
    e.preventDefault();
    boutonselectionner = $(this);

    var idanal = boutonselectionner.attr('data-id');
    var letrimestre = $("#combotrimestre option:selected");
    var montantdepense = $("#ajoutmontantexecute");

    if (letrimestre.val() === "0") {
        letrimestre.notify(
            "Veuillez selectionner le trimestre",
            {position: "up"},
            "warn"
        );
        letrimestre.focus();

    } else {
        if (montantdepense.val() === "") {
            montantdepense.notify(
                "Veuillez saisir le montant",
                {position: "up"},
                "warn"
            );
            montantdepense.focus();

        } else {
            // console.log("ok1 ");
            letrimestre.attr('disabled', true);
            montantdepense.attr('disabled', true);
            boutonselectionner.button('loading');
            $.ajax({
                url: lienenregistrementexecution,
                type: "POST",
                data: {
                    'idanalyse': idanal,
                    'trimestre': letrimestre.val(),
                    'montant': montantdepense.val(),
                },
                success: function (data) {

                    // console.log(data.statut);
                    if (data.statut === "true") {

                        swalWithBootstrapButtons.fire(
                            'Validé !',
                            'Dépense enregistrée avec succes !',
                            'success'
                        );

                        $("#liste-depense").append("<tr>\n" +
                            "                                        <th><font style=\"vertical-align: inherit;width: 40px;\"><font style=\"vertical-align: inherit;\">"+letrimestre.text()+"</font></font></th>\n" +
                            "                                        <th><font style=\"vertical-align: inherit;width: 50px;text-align: right:\"><font style=\"vertical-align: inherit;\">"+montantdepense.val()+"</font></font></th>\n" +
                            "                                        <th><font style=\"vertical-align: inherit;width: 10px;\"><font style=\"vertical-align: inherit;\">\n" +
                            "                                        <a data-id="+data.codeexec+" type=\"button\" class=\"btn btn-danger deletedligne\">\n" +
                            "                                            <i class=\"fa fa-trash\"></i></a> </font></font></th>\n" +
                            "                                        \n" +
                            "                                    </tr>");

                        deleteligne();

                        // var nouveausoldeexecute = new Number(montantdepense.val());
                        // console.log($("#montantexecute").val());
                        // console.log(montantdepense.val());

                        $("#montantexecute").val(parseFloat($("#montantexecute").val())+parseFloat(montantdepense.val()));
                        $("#disponible").val(parseFloat($("#montantoctroye").val())-parseFloat($("#montantexecute").val()));
                        letrimestre.val("");
                        montantdepense.val("");



                    } else {

                        // console.log(data.statut)
                        if (data.statut === "Disponible non suffisant") {

                            swalWithBootstrapButtons.fire(
                                'Alert !',
                                'Disponible non suffisant !',
                                'error'
                            );

                        } else {

                            swalWithBootstrapButtons.fire(
                                'Alert !',
                                'Erreur si cette erreur persite ! Veuillez contacter l\'administrateur ou réessayer !',
                                'error'
                            );

                        }

                    }
                    //modalTraitement.modal('hide');
                    letrimestre.removeAttr('disabled');
                    montantdepense.removeAttr('disabled');
                    boutonselectionner.button('reset');

                },
                error: function (error) {
                    letrimestre.removeAttr('disabled');
                    montantdepense.removeAttr('disabled');
                    boutonselectionner.button('reset');

                }
            })

        }

    }

});


$("#affichedetail").on('click', function (e) {

    e.preventDefault();
    boutonselectionner = $(this);

    if(boutonselectionner.text() === "Consulter Detail"){

        $("#detailexecution").show("slow");
        boutonselectionner.text("Cacher Detail");
    }else{

        $("#detailexecution").hide("slow");
        boutonselectionner.text("Consulter Detail");
    }

});

$("#tableligneexecution").on('click', '.btndetailanalyse', function (e) {

    e.preventDefault();

    boutonselectionner = $(this);

    var idanalyse = boutonselectionner.attr('data-id');
    boutonselectionner.button('loading');
    $.ajax({
        url: lienchargementinfo,
        type: "GET",
        data: {
            'idanalyse': idanalyse,
        },
        success: function (data) {
            boutonselectionner.button('reset');
            $("#montantoctroye").val(parseFloat(data.activAnalyse['budMont']));
            $("#montantexecute").val(parseFloat(data.montantExecute));
            $("#disponible").val(parseFloat(data.disponible));
            $("#ajoutmontant").attr('data-id',idanalyse);
            $("#title-modal").text(data.activAnalyse['cibCod']+' - '+data.activAnalyse['activLib']);
            // console.log(data);
            $("#liste-depense").html("                                    <tr>\n" +
                "                                        <th><font style=\"vertical-align: inherit;width: 40px;\"><font style=\"vertical-align: inherit;\">Trimestre</font></font></th>\n" +
                "                                        <th><font style=\"vertical-align: inherit;width: 50px;text-align: right;\"><font style=\"vertical-align: inherit;\">Montant</font></font></th>\n" +
                "                                        <th><font style=\"vertical-align: inherit;width: 10px;\"><font style=\"vertical-align: inherit;\">Actions</font></font></th>\n" +
                "\n" +
                "                                    </tr>");

            data.listeExecutionAnalyse.forEach(function (g) {
                // console.log(g)
                $("#liste-depense").append("<tr>\n" +
                    "                                        <th><font style=\"vertical-align: inherit;width: 40px;\"><font style=\"vertical-align: inherit;\">"+g.trimLib+"</font></font></th>\n" +
                    "                                        <th><font style=\"vertical-align: inherit;width: 50px;text-align: right;\"><font style=\"vertical-align: inherit;\">"+addCommas(g.execMont)+"</font></font></th>\n" +
                    "                                        <th><font style=\"vertical-align: inherit;width: 10px;\"><font style=\"vertical-align: inherit;\">\n" +
                    "                                        <a data-id="+g.id+" type=\"button\" class=\"btn btn-danger deletedligne\">\n" +
                    "                                            <i class=\"fa fa-trash\"></i></a> </font></font></th>\n" +
                    "                                        \n" +
                    "                                    </tr>");
            });

            deleteligne();
        },
        error: function (error) {
            btn.button('reset');
            swal({
                title: "Alert!",
                icon: "error",
                button: false,
                text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                timer: 1500
            });
        }

    });
    ModalDetailAnalyse.modal('show');


});

$("#valider").on('click', function (e) {
    e.preventDefault();

    // console.log('ok1')
    boutonselectionner = $(this);

    swalWithBootstrapButtons.fire({
        title: 'Confirmation !',
        text: "Voulez-vous vraiment enregistrer ces données ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui !',
        cancelButtonText: 'Non !',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
        // boutonselectionner.button('loading');
        if (document.getElementById("file").value != "") {
            boutonselectionner.button('loading');
            var form = $('#upload-form')[0];
            var data = new FormData(form);
            data.append("text", document.getElementById("text").value);
            $.ajax({
                    url: lienenregistrement,
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (respons) {

                        if(respons['statut'] == "Vérifier le fichier"){
                            swalWithBootstrapButtons.fire(
                                'Erreur fichier !',
                                'Veuillez sélectionnée a nouveau le fichier !',
                                'error'
                            );
                        }else{
                            tableadddonne.clear();
                            swalWithBootstrapButtons.fire(
                                'Validé !',
                                'Donnée enregistrer avec succes !',
                                'success'
                            );
                            document.getElementById("file").value = "";
                        }

                        // // // table.clear();
                        //  $.each(respons['success'], function (index, element) {
                        //      var tablerow = tableadddonne.row.add([element.code, element.libelle, element.montant]).node();
                        // //     /*$(tablerow).attr('data-id', element.ID);*/
                        //  });
                        // table.draw();
                        boutonselectionner.button('reset');
                        // console.log(respons['success']);
                        // console.log(respons[0].code)

                        // // // table.clear();
                        //  $.each(respons['success'], function (index, element) {
                        //      var tablerow = tableadddonne.row.add([element.code, element.libelle, element.montant]).node();
                        // //     /*$(tablerow).attr('data-id', element.ID);*/
                        //  });
                        // table.draw();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        // alert(xhr.status);
                        // alert(thrownError);
                        swalWithBootstrapButtons.fire(
                            'Erreur fichier !',
                            'Veuillez vérifier a nouveau le fichier !',
                            'error'
                        );
                        boutonselectionner.button('reset');
                    }
                }
            );
        }else{
            $.notify('Veuillez sélectionner un fichier !', 'error');
        }


    }
});

});




function deleteligne() {
    $(".deletedligne").on('click', function (e) {
        e.preventDefault();

        // console.log('ok1')
        boutonselectionner = $(this);
        var idexect = boutonselectionner.attr('data-id');

        // console.log(idexect)
        swalWithBootstrapButtons.fire({
            title: 'Confirmation !',
            text: "Voulez-vous vraiment supprimer ces données ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Oui !',
            cancelButtonText: 'Non !',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                boutonselectionner.button('loading');
                $.ajax({
                        url: liensupressionexecution,
                        type: 'POST',
                        data: {
                            'idexect' : idexect
                        },
                        success: function (data) {

                            if(data['statut'] == "Validé"){
                                swalWithBootstrapButtons.fire(
                                    'Erreur !',
                                    'Cette ligne est déjà validée !',
                                    'error'
                                );
                            }else{
                                swalWithBootstrapButtons.fire(
                                    'Validé !',
                                    'Donnée supprimée avec succes !',
                                    'success'
                                );

                                // console.log($("#montantexecute").val(),data['montant']);
                                $("#montantexecute").val(parseFloat($("#montantexecute").val())-parseFloat(data['montant']));
                                $("#disponible").val(parseFloat($("#disponible").val())+parseFloat(data['montant']));
                                boutonselectionner.closest('th').closest('tr').fadeOut(2000, function () {
                                    boutonselectionner.closest('th').closest('tr').remove();
                                });
                            }

                            boutonselectionner.button('reset');
                            //  });
                            // table.draw();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            // alert(xhr.status);
                            // alert(thrownError);
                            swalWithBootstrapButtons.fire(
                                'Erreur !',
                                'Veuillez réessayée !',
                                'error'
                            );
                            boutonselectionner.button('reset');
                        }
                    }
                );


            }
        });

    });
}


//
// $(function () {
//         document.getElementById('progress-value').style = "width: 0%";
//         document.getElementById('upload-form').addEventListener('submit', onSubmit);
//
//         function onSubmit(event) {
//             event.preventDefault();
//             resetAlertBox();
//             var formData = new FormData();
//             formData.append("upload[file]", document.getElementById("file").files[0]);
//             var xhr = new XMLHttpRequest();
//             xhr.open("POST", lienchargement);
//             xhr.addEventListener('load', onRequestComplete, false);
//             xhr.upload.addEventListener("load", onUploadComplete, false);
//             xhr.upload.addEventListener("progress", onUploadProgress, false);
//             xhr.send(formData);
//         }
//
//         function onUploadProgress(event) {
//             if (event.lengthComputable) {
//                 $('#progress-box').css('display', 'block');
//                 var percentComplete = event.loaded / event.total;
//                 document.getElementById('progress-value').style = "width: " + parseFloat(percentComplete * 100).toFixed(2) + "%";
//             }
//         }
//
//         function onUploadComplete() {
//             $('#upload-complete-box').css('display', 'block');
//         }
//
//         function onRequestComplete(event) {
//             if (201 === event.explicitOriginalTarget.status) {
//                 var response = JSON.parse(event.explicitOriginalTarget.response);
//                 $('#upload-complete').append('Upload complete: <a href="/' + response.key + '">download key ' + response.key + '</a>');
//                 return;
//             }
//             var errors = JSON.parse(event.explicitOriginalTarget.response);
//             $('#errors-box').css('display', 'block');
//             $('#upload-complete-box').css('display', 'none');
//             var list = document.createElement('ul');
//             for (var i = 0; i < errors.length; i++) {
//                 var item = document.createElement('li');
//                 item.appendChild(document.createTextNode(errors[i]));
//                 list.appendChild(item);
//             }
//             var title = document.createElement('span');
//             title.textContent = "Error found";
//             document.getElementById('errors').appendChild(title);
//             document.getElementById('errors').appendChild(list);
//         }
//
//         function resetAlertBox() {
//             $('#errors-box').css('display', 'none');
//             $('#upload-complete-box').css('display', 'none');
//             $('#progress-box').css('display', 'none');
//         }
//     }
// );
