var tableadddonne = $("#tablelistedonnee").DataTable({
    'order': [[0, 'desc']],
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun résultat trouvé",
        "sEmptyTable": "Aucune donnée disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucune ligne affichée",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant", "sSortDescending": ": Trier par ordre décroissant"
        }
    }

});


// var form = document.getElementById("myform");
// or with jQuery
//var form = $("#myform")[0];

// $('#fichier').on('click', function (e) {
//     e.preventDefault();

$("#charger").on('click', function (e) {
    e.preventDefault();

    // console.log('ok1')
    boutonselectionner = $(this);
    if (document.getElementById("file").value != "") {
        boutonselectionner.button('loading');

        var form = $('#upload-form')[0];
        var data = new FormData(form);
        data.append("text", document.getElementById("text").value);
        $.ajax({
                url: lienchargement,
                type: 'POST',
                enctype: 'multipart/form-data',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (respons) {

                    // console.log(respons['success']);
                    // console.log(respons[0].code)

                    if(respons['statut'] == "Vérifier le fichier"){
                        $.notify('Erreur fichier ! Veuillez sélectionnée a nouveau le fichier', 'error');

                    }else{
                        tableadddonne.clear();

                        respons['statut'].forEach(function (g) {
                            tableadddonne.row.add([
                                g.code,
                                g.libelle,
                                g.montant
                            ])
                            // console.log(g.code);
                        });
                        tableadddonne.draw();

                        $.notify('Fichier chargé avec succes !', 'success');
                    }

                    // // // table.clear();
                    //  $.each(respons['success'], function (index, element) {
                    //      var tablerow = tableadddonne.row.add([element.code, element.libelle, element.montant]).node();
                    // //     /*$(tablerow).attr('data-id', element.ID);*/
                    //  });
                    // table.draw();
                    boutonselectionner.button('reset');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status);
                    // alert(thrownError);
                    swalWithBootstrapButtons.fire(
                        'Erreur fichier !',
                        'Veuillez vérifier a nouveau le fichier !',
                        'error'
                    );
                    boutonselectionner.button('reset');
                }


            }
        );
    }else{
        $.notify('Veuillez sélectionner un fichier !', 'error');
    }

});


$("#valider").on('click', function (e) {
    e.preventDefault();

    // console.log('ok1')
    boutonselectionner = $(this);

    swalWithBootstrapButtons.fire({
        title: 'Confirmation !',
        text: "Voulez-vous vraiment enregistrer ces données ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui !',
        cancelButtonText: 'Non !',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
        // boutonselectionner.button('loading');
        if (document.getElementById("file").value != "") {
            boutonselectionner.button('loading');
            var form = $('#upload-form')[0];
            var data = new FormData(form);
            data.append("text", document.getElementById("text").value);
            $.ajax({
                    url: lienenregistrement,
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (respons) {

                        if(respons['statut'] == "Vérifier le fichier"){
                            swalWithBootstrapButtons.fire(
                                'Erreur fichier !',
                                'Veuillez sélectionnée a nouveau le fichier !',
                                'error'
                            );
                        }else{
                            tableadddonne.clear();
                            swalWithBootstrapButtons.fire(
                                'Validé !',
                                'Données enregistrées avec succès !',
                                'success'
                            );
                            document.getElementById("file").value = "";
                        }

                        // // // table.clear();
                        //  $.each(respons['success'], function (index, element) {
                        //      var tablerow = tableadddonne.row.add([element.code, element.libelle, element.montant]).node();
                        // //     /*$(tablerow).attr('data-id', element.ID);*/
                        //  });
                        // table.draw();
                        boutonselectionner.button('reset');
                        // console.log(respons['success']);
                        // console.log(respons[0].code)

                        // // // table.clear();
                        //  $.each(respons['success'], function (index, element) {
                        //      var tablerow = tableadddonne.row.add([element.code, element.libelle, element.montant]).node();
                        // //     /*$(tablerow).attr('data-id', element.ID);*/
                        //  });
                        // table.draw();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        // alert(xhr.status);
                        // alert(thrownError);
                        swalWithBootstrapButtons.fire(
                            'Erreur fichier !',
                            'Veuillez vérifier a nouveau le fichier !',
                            'error'
                        );
                        boutonselectionner.button('reset');
                    }
                }
            );
        }else{
            $.notify('Veuillez sélectionner un fichier !', 'error');
        }


    }
});

});
//
// $(function () {
//         document.getElementById('progress-value').style = "width: 0%";
//         document.getElementById('upload-form').addEventListener('submit', onSubmit);
//
//         function onSubmit(event) {
//             event.preventDefault();
//             resetAlertBox();
//             var formData = new FormData();
//             formData.append("upload[file]", document.getElementById("file").files[0]);
//             var xhr = new XMLHttpRequest();
//             xhr.open("POST", lienchargement);
//             xhr.addEventListener('load', onRequestComplete, false);
//             xhr.upload.addEventListener("load", onUploadComplete, false);
//             xhr.upload.addEventListener("progress", onUploadProgress, false);
//             xhr.send(formData);
//         }
//
//         function onUploadProgress(event) {
//             if (event.lengthComputable) {
//                 $('#progress-box').css('display', 'block');
//                 var percentComplete = event.loaded / event.total;
//                 document.getElementById('progress-value').style = "width: " + parseFloat(percentComplete * 100).toFixed(2) + "%";
//             }
//         }
//
//         function onUploadComplete() {
//             $('#upload-complete-box').css('display', 'block');
//         }
//
//         function onRequestComplete(event) {
//             if (201 === event.explicitOriginalTarget.status) {
//                 var response = JSON.parse(event.explicitOriginalTarget.response);
//                 $('#upload-complete').append('Upload complete: <a href="/' + response.key + '">download key ' + response.key + '</a>');
//                 return;
//             }
//             var errors = JSON.parse(event.explicitOriginalTarget.response);
//             $('#errors-box').css('display', 'block');
//             $('#upload-complete-box').css('display', 'none');
//             var list = document.createElement('ul');
//             for (var i = 0; i < errors.length; i++) {
//                 var item = document.createElement('li');
//                 item.appendChild(document.createTextNode(errors[i]));
//                 list.appendChild(item);
//             }
//             var title = document.createElement('span');
//             title.textContent = "Error found";
//             document.getElementById('errors').appendChild(title);
//             document.getElementById('errors').appendChild(list);
//         }
//
//         function resetAlertBox() {
//             $('#errors-box').css('display', 'none');
//             $('#upload-complete-box').css('display', 'none');
//             $('#progress-box').css('display', 'none');
//         }
//     }
// );
