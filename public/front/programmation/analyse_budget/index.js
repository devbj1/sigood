var tableaddanalyse = $("#tablelisteanalyse").DataTable({
    'order': [[0, 'desc']],
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun résultat trouvé",
        "sEmptyTable": "Aucune donnée disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucune ligne affichée",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant", "sSortDescending": ": Trier par ordre décroissant"
        }
    }

});


// var form = document.getElementById("myform");
// or with jQuery
//var form = $("#myform")[0];

// $('#fichier').on('click', function (e) {
//     e.preventDefault();

$("#charger-analyse").on('click', function (e) {
    e.preventDefault();

    // console.log('ok1')
    boutonselectionner = $(this);
    if (document.getElementById("file").value != "") {
        boutonselectionner.button('loading');

        var form = $('#upload-form-analyse')[0];
        var data = new FormData(form);
        data.append("text", document.getElementById("textanalyse").value);
        $.ajax({
                url: lienchargementanalyse,
                type: 'POST',
                enctype: 'multipart/form-data',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (respons) {

                    // console.log(respons['success']);
                    // console.log(respons[0].code)

                    if(respons['statut'] == "Vérifier le fichier"){
                        $.notify('Erreur fichier ! Veuillez sélectionnée a nouveau le fichier', 'error');

                    }else{
                        tableaddanalyse.clear();

                        respons['statut'].forEach(function (g) {
                            tableaddanalyse.row.add([
                                g.cible,
                                g.activite,
                                g.trimestre
                            ])
                            // console.log(g.code);
                        });
                        tableaddanalyse.draw();

                        $.notify('Fichier chargé avec succes !', 'success');
                    }

                    boutonselectionner.button('reset');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status);
                    // alert(thrownError);
                    swalWithBootstrapButtons.fire(
                        'Erreur fichier !',
                        'Veuillez vérifier a nouveau le fichier !',
                        'error'
                    );
                    boutonselectionner.button('reset');
                }
            }
        );
    }else{
        $.notify('Veuillez sélectionner un fichier !', 'error');
    }

});


$("#valider").on('click', function (e) {
    e.preventDefault();

    // console.log('ok1')
    boutonselectionner = $(this);

    swalWithBootstrapButtons.fire({
        title: 'Confirmation !',
        text: "Voulez-vous vraiment enregistrer ces données ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui !',
        cancelButtonText: 'Non !',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
        // boutonselectionner.button('loading');
        if (document.getElementById("file").value != "") {
            boutonselectionner.button('loading');
            var form = $('#upload-form-analyse')[0];
            var data = new FormData(form);
            data.append("text", document.getElementById("textanalyse").value);
            $.ajax({
                    url: lienenregistrementanalyse,
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (respons) {

                        if(respons['statut'] == "Vérifier le fichier"){
                            swalWithBootstrapButtons.fire(
                                'Erreur fichier !',
                                'Veuillez sélectionnée a nouveau le fichier !',
                                'error'
                            );
                        }else{
                            tableaddanalyse.clear();
                            swalWithBootstrapButtons.fire(
                                'Validé !',
                                'Données enregistrées avec succès !',
                                'success'
                            );
                            document.getElementById("file").value = "";
                        }

                        boutonselectionner.button('reset');

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        // alert(xhr.status);
                        // alert(thrownError);
                        swalWithBootstrapButtons.fire(
                            'Erreur fichier !',
                            'Veuillez vérifier a nouveau le fichier !',
                            'error'
                        );
                        boutonselectionner.button('reset');
                    }
                }
            );
        }else{
            $.notify('Veuillez sélectionner un fichier !', 'error');
        }


    }
});

});


$("#annulerchargement").on('click', function (e) {
    e.preventDefault();
    // console.log('ok1')
    // console.log('ok1')
    boutonselectionner = $(this);

    swalWithBootstrapButtons.fire({
        title: 'Confirmation !',
        text: "Voulez-vous vraiment annulée ces données ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui !',
        cancelButtonText: 'Non !',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {


            swalWithBootstrapButtons.fire({
                title: 'Confirmation !',
                text: "Etes vous sur vous devriez les rechargées ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Oui !',
                cancelButtonText: 'Non !',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {

                    boutonselectionner.button('loading');

                    $.ajax({
                            url: lienannulleanalyse,
                            type: 'POST',
                            // enctype: 'multipart/form-data',
                            // data: data,
                            // processData: false,
                            // contentType: false,
                            // cache: false,
                            // timeout: 600000,
                            success: function (respons) {

                                swalWithBootstrapButtons.fire(
                                    'Annulation !',
                                    'Donnée annulée avec succes !',
                                    'success'
                                );

                                tableaddanalyse.clear();

                                boutonselectionner.button('reset');

                            }
                        }
                    );


                }
            });



        }
    });
});
//
// $(function () {
//         document.getElementById('progress-value').style = "width: 0%";
//         document.getElementById('upload-form').addEventListener('submit', onSubmit);
//
//         function onSubmit(event) {
//             event.preventDefault();
//             resetAlertBox();
//             var formData = new FormData();
//             formData.append("upload[file]", document.getElementById("file").files[0]);
//             var xhr = new XMLHttpRequest();
//             xhr.open("POST", lienchargement);
//             xhr.addEventListener('load', onRequestComplete, false);
//             xhr.upload.addEventListener("load", onUploadComplete, false);
//             xhr.upload.addEventListener("progress", onUploadProgress, false);
//             xhr.send(formData);
//         }
//
//         function onUploadProgress(event) {
//             if (event.lengthComputable) {
//                 $('#progress-box').css('display', 'block');
//                 var percentComplete = event.loaded / event.total;
//                 document.getElementById('progress-value').style = "width: " + parseFloat(percentComplete * 100).toFixed(2) + "%";
//             }
//         }
//
//         function onUploadComplete() {
//             $('#upload-complete-box').css('display', 'block');
//         }
//
//         function onRequestComplete(event) {
//             if (201 === event.explicitOriginalTarget.status) {
//                 var response = JSON.parse(event.explicitOriginalTarget.response);
//                 $('#upload-complete').append('Upload complete: <a href="/' + response.key + '">download key ' + response.key + '</a>');
//                 return;
//             }
//             var errors = JSON.parse(event.explicitOriginalTarget.response);
//             $('#errors-box').css('display', 'block');
//             $('#upload-complete-box').css('display', 'none');
//             var list = document.createElement('ul');
//             for (var i = 0; i < errors.length; i++) {
//                 var item = document.createElement('li');
//                 item.appendChild(document.createTextNode(errors[i]));
//                 list.appendChild(item);
//             }
//             var title = document.createElement('span');
//             title.textContent = "Error found";
//             document.getElementById('errors').appendChild(title);
//             document.getElementById('errors').appendChild(list);
//         }
//
//         function resetAlertBox() {
//             $('#errors-box').css('display', 'none');
//             $('#upload-complete-box').css('display', 'none');
//             $('#progress-box').css('display', 'none');
//         }
//     }
// );
