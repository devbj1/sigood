var tableadddonnevalide = $("#tablelistedonneeavalide").DataTable({
    'order': [[0, 'desc']],
    // 'pageLength': 50,
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun résultat trouvé",
        "sEmptyTable": "Aucune donnée disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucune ligne affichée",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant", "sSortDescending": ": Trier par ordre décroissant"
        }
    }

});


$("#validerchargement").on('click', function (e) {
    e.preventDefault();

    // console.log('ok2')
    boutonselectionner = $(this);

    swalWithBootstrapButtons.fire({
        title: 'Confirmation !',
        text: "Voulez-vous vraiment enregistrer ces données ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui !',
        cancelButtonText: 'Non !',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
        // boutonselectionner.button('loading');
            boutonselectionner.button('loading');
            $.ajax({
                    url: lienvalidationelaboration,
                    type: 'POST',
                    // enctype: 'multipart/form-data',
                    // data: data,
                    // processData: false,
                    // contentType: false,
                    // cache: false,
                    // timeout: 600000,
                    success: function (respons) {

                        swalWithBootstrapButtons.fire(
                            'Validé !',
                            'Elaboration validée avec succes !',
                            'success'
                        );

                        tableadddonnevalide.clear();
                        boutonselectionner.button('reset');
                    }
                }
            );
    }
});

});


$("#annulerchargement").on('click', function (e) {
    e.preventDefault();
    // console.log('ok1')
    // console.log('ok1')
    boutonselectionner = $(this);

    swalWithBootstrapButtons.fire({
        title: 'Confirmation !',
        text: "Voulez-vous vraiment annulée ces données ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui !',
        cancelButtonText: 'Non !',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {


        swalWithBootstrapButtons.fire({
            title: 'Confirmation !',
            text: "Etes vous sur vous devriez les rechargées ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Oui !',
            cancelButtonText: 'Non !',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {

            boutonselectionner.button('loading');

            $.ajax({
                    url: lienannulleelaboration,
                    type: 'POST',
                    // enctype: 'multipart/form-data',
                    // data: data,
                    // processData: false,
                    // contentType: false,
                    // cache: false,
                    // timeout: 600000,
                    success: function (respons) {

                        swalWithBootstrapButtons.fire(
                            'Annulation !',
                            'Donnée annulée avec succes !',
                            'success'
                        );

                        tableadddonnevalide.clear();

                        boutonselectionner.button('reset');

                    }
                }
            );


        }
        });



    }
});

});


