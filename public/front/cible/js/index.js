

var table = $("#tablelistecible").DataTable({
    'order': [[0, 'desc']],
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun résultat trouvé",
        "sEmptyTable": "Aucune donnée disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucune ligne affichée",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant", "sSortDescending": ": Trier par ordre décroissant"
        }
    },
    'dom': 'Bfrtip',
    'buttons': [
        {
            extend: 'copyHtml5',
            exportOptions: {
                columns: [ 0, 1]
            }
        },
        {
            extend: 'excelHtml5',
            exportOptions: {
                columns: [ 0, 1]
            }
        },
        'csv',
        {
            extend: 'pdfHtml5',
            title : 'Liste des cibles',
            exportOptions: {
                columns: [ 0, 1]
            }
        },
        'print'
    ],
    'extend': 'pdfHtml5',
    'title': 'Liste des cibles',
    'text': 'The text',
    orientation: 'landscape',
    'pageSize': 'A4',
    'exportOptions': {
        columns: [ 0, 1]
    },
    customize: function ( doc ) {
        doc.content[1].table.widths = [
            '20%',
            '80%'
        ]
    }


});